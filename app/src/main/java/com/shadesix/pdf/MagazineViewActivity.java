package com.shadesix.pdf;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.shadesix.pdf.adapters.ExploreAdapter;
import com.shadesix.pdf.models.GetArticleModel;
import com.shadesix.pdf.models.GetArticleModel2;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class MagazineViewActivity extends AppCompatActivity {

    static String TAG = GetAllArticleListActivity.class.getSimpleName();
    RecyclerView exploreList;
    ExploreAdapter dealadapter;
    public static ArrayList<GetArticleModel> articleList = new ArrayList<GetArticleModel>();
    String magazineId,coverPath;
    Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private AppBarLayout appBarLayout = null;
    ImageView grid,list,magCover;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magazine_view);

        exploreList = (RecyclerView) findViewById(R.id.explore_list);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        grid = (ImageView) findViewById(R.id.img_square);
        list = (ImageView) findViewById(R.id.img_sort);
        magCover = (ImageView) findViewById(R.id.img_main);

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticleList();
            }
        });

        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticleGrid();
            }
        });

        magCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(GetAllArticleListActivity.this,PDFActivity.class));
            }
        });

        magazineId = getIntent().getStringExtra("magazine_id");
        Log.e(TAG,"mag ID "+magazineId);

        getArticleList();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(verticalOffset == 0){
                    collapsingToolbarLayout.setTitle("");
                    toolbar.setVisibility(View.GONE);
                }else {
                    toolbar.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void getArticleList(){
//        Toast.makeText(GetAllArticleListActivity.this, "Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("magazine_id", magazineId);
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_articles.php", params, new MagazineViewActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            GetArticleModel2 model = new Gson().fromJson(new String(responseBody), GetArticleModel2.class);
            if (model.success == 1) {
                try {
                    for (GetArticleModel mydealmodel : model.articleDetail) {
                        articleList.add(mydealmodel);
//                        dealadapter = new ExploreAdapter(articleList, MagazineViewActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MagazineViewActivity.this, 1);
                        exploreList.setLayoutManager(mLayoutManager);
                        exploreList.setAdapter(dealadapter);
                    }

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    public void getArticleGrid(){
//        Toast.makeText(GetAllArticleListActivity.this, "Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("magazine_id", magazineId);
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_articles.php", params, new MagazineViewActivity.GetResponsehandler2());
    }

    public class GetResponsehandler2 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            GetArticleModel2 model = new Gson().fromJson(new String(responseBody), GetArticleModel2.class);
            if (model.success == 1) {
                try {
                    for (GetArticleModel mydealmodel : model.articleDetail) {
                        articleList.add(mydealmodel);
//                        dealadapter = new ExploreAdapter(articleList, MagazineViewActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MagazineViewActivity.this, 2);
                        exploreList.setLayoutManager(mLayoutManager);
                        exploreList.setAdapter(dealadapter);
                    }

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }
}
