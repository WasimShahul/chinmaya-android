package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.adapters.ExploreAdapter;
import com.shadesix.pdf.adapters.NotificationAdapter;
import com.shadesix.pdf.fragments.CompassFragment;
import com.shadesix.pdf.models.ExploreModel;
import com.shadesix.pdf.models.ExploreModel2;
import com.shadesix.pdf.models.NotficationModel;
import com.shadesix.pdf.models.NotificationModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity {

    static  String TAG = NotificationActivity.class.getSimpleName();
    RecyclerView notifyList;
    public static ArrayList<NotficationModel> notificationArrayList = new ArrayList<NotficationModel>();
    NotificationAdapter dealadapter;
    TextView noNotification;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        notifyList = (RecyclerView) findViewById(R.id.notification_list);
        noNotification = (TextView) findViewById(R.id.no_notification_txt);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        getNotoficationList();
    }

    public void getNotoficationList(){
        progressDialog.setMessage("loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(NotificationActivity.this, Constant.PARAM_VALID_ID));
        Log.e(TAG,"userid "+Utils.getFromUserDefaults(NotificationActivity.this, Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_new_notification.php", params, new NotificationActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            NotificationModel2 model = new Gson().fromJson(new String(responseBody), NotificationModel2.class);
            if (model.success == 1) {
                try {
                    notificationArrayList.clear();
                    for (NotficationModel atl : model.notifyDetail) {
                        notificationArrayList.add(atl);
                        dealadapter = new NotificationAdapter(notificationArrayList,NotificationActivity.this);
                        Log.e(TAG,"Notification "+notificationArrayList.size());

                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(NotificationActivity.this, 1);
                        notifyList.setLayoutManager(mLayoutManager);
                        notifyList.setAdapter(dealadapter);
                    }

                }catch (Exception e){

                }
            }
            else{
                noNotification.setVisibility(View.VISIBLE);
                Toast.makeText(NotificationActivity.this,model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(NotificationActivity.this,"Unable to get article title list.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(NotificationActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(NotificationActivity.this,MainActivity.class));
        finish();
    }
}
