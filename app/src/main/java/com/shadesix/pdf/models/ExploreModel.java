package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 28-03-2017.
 */

public class ExploreModel {

    @SerializedName("id")
    public String id;

    @SerializedName("title")
    public String title;

    @SerializedName("cover_path")
    public String cover_path;

    @SerializedName("content")
    public String content;

}
