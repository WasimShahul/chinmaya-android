package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Shade Six on 08-04-2017.
 */

public class LoginModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("result")
    public ArrayList<LoginModel> userDetail;
}
