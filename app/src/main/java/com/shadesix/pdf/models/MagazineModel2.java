package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Shade Six on 10-04-2017.
 */

public class MagazineModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("magazines")
    public ArrayList<MagazineModel> magazineDetail;
}
