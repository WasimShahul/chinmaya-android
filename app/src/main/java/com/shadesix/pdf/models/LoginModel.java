package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 08-04-2017.
 */

public class LoginModel {
    @SerializedName("user_id")
    public String id;

    @SerializedName("fname")
    public String first_name;

    @SerializedName("lname")
    public String last_name;

    @SerializedName("email")
    public String email;

    @SerializedName("dob")
    public String date_of_birth;

    @SerializedName("age")
    public String age;

    @SerializedName("gender")
    public String gender;

    @SerializedName("city")
    public String city;

    @SerializedName("state")
    public String state;

    @SerializedName("country")
    public String country;
}
