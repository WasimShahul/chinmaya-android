package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shade6 on 01/05/17.
 */

public class NotficationModel {

    public String id;

    @SerializedName("notify_text")
    public String notify_text;

    @SerializedName("type")
    public String notify_type;
}
