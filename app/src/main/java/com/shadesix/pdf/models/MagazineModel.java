package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 10-04-2017.
 */

public class MagazineModel {
    @SerializedName("id")
    public String id;

    @SerializedName("title")
    public String title;

    @SerializedName("publisher")
    public String publisher;

    @SerializedName("publishing_date")
    public String publishing_date;

    @SerializedName("content")
    public String content;

    @SerializedName("pdf_path")
    public String pdf_path;

    @SerializedName("cover_path")
    public String cover_path;

    @SerializedName("image2")
    public String image2;

    @SerializedName("image3")
    public String image3;

    @SerializedName("volume")
    public String volume;

    @SerializedName("issue")
    public String issue;

    @SerializedName("price")
    public String price;

    @SerializedName("created_time")
    public String created_time;
}
