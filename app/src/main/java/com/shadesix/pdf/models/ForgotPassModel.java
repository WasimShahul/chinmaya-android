package com.example.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shade6 on 18/05/17.
 */

public class ForgotPassModel {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;
}
