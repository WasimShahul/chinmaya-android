package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class LikesModel {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("count")
    public String count;
}
