package com.shadesix.pdf.models;

/**
 * Created by Shade Six on 27-03-2017.
 */

public class ArticleModel {
    public String title,description;

    public ArticleModel(){

    }

    public ArticleModel(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
