package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class GetIsLikedModel {
    @SerializedName("is_liked")
    public String is_liked;

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;
}
