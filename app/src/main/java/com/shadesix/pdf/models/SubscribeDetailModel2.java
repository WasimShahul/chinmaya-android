package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 08-04-2017.
 */

public class SubscribeDetailModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("type")
    public String type;

    @SerializedName("remaining")
    public String remaining;
}
