package com.shadesix.pdf.models;

/**
 * Created by Shade Six on 13-04-2017.
 */

public class GameEntity {
    public int imageResId;
    public int titleResId;

    public GameEntity (int imageResId, int titleResId){
        this.imageResId = imageResId;
        this.titleResId = titleResId;
    }
}