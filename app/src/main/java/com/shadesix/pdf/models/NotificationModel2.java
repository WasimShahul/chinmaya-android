package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shade6 on 01/05/17.
 */

public class NotificationModel2 {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("notifications")
    public ArrayList<NotficationModel> notifyDetail;
}
