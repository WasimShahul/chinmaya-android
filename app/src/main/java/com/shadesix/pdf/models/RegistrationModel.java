package com.shadesix.pdf.models;

/**
 * Created by Shade Six on 29-03-2017.
 */

public class RegistrationModel {

    public String userId,fname,lname,email,dob,country,state,city,registeredDate;

    public RegistrationModel(){

    }

    public RegistrationModel(String userId, String fname, String lname, String email, String dob, String country, String state,
                             String city, String registeredDate) {
        this.userId = userId;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.dob = dob;
        this.country = country;
        this.state = state;
        this.city = city;
        this.registeredDate = registeredDate;
    }
}
