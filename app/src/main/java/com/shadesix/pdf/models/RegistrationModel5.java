package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 07-04-2017.
 */

public class RegistrationModel5 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;
}
