package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 08-04-2017.
 */

public class SubscribeDetailModel {
    @SerializedName("subDate")
    public String subDate;

    @SerializedName("endDate")
    public String endDate;

    @SerializedName("subType")
    public String subType;

    @SerializedName("email")
    public String email;
}
