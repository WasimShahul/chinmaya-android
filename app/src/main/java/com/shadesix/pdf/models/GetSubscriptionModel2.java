package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class GetSubscriptionModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("articles")
    public ArrayList<GetSubscriptionModel> subcriptionDetail;
}
