package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class LikedModel {
    @SerializedName("id")
    public String id;

    @SerializedName("article_id")
    public String article_id;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("cover_path")
    public String cover_path;

    @SerializedName("image2")
    public String image2;

    @SerializedName("image3")
    public String image3;

    @SerializedName("author_name")
    public String author_name;

    @SerializedName("published_date")
    public String published_date;

    @SerializedName("edition_no")
    public String edition_no;


    @SerializedName("created_time")
    public String created_time;

    @SerializedName("article_likes")
    public String article_likes;

    @SerializedName("is_liked")
    public String is_liked;
}
