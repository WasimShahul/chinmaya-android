package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Shade Six on 12-04-2017.
 */

public class QuoteModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("quotes")
    public ArrayList<QuoteModel> quote;
}
