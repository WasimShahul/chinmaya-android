package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 12-04-2017.
 */

public class QuoteModel {
    @SerializedName("content")
    public String content;
}
