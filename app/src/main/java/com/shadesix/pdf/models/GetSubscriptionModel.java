package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class GetSubscriptionModel {

    @SerializedName("id")
    public String id;

    @SerializedName("subscription_name")
    public String subscription_name;

    @SerializedName("duration")
    public String duration;

    @SerializedName("price")
    public String price;

    @SerializedName("usd")
    public String usd;

    @SerializedName("created_time")
    public String created_time;

}
