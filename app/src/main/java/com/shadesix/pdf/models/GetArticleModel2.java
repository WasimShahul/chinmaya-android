package com.shadesix.pdf.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class GetArticleModel2 {
    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("articles")
    public ArrayList<GetArticleModel> articleDetail;
}
