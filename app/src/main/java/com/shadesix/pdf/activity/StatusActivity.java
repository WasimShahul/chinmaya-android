package com.shadesix.pdf.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.MainActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.SplashScreenActivity;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class StatusActivity extends Activity {

	ProgressDialog progressDialog;
	private static int SPLASH_TIME_OUT = 3000;
	
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_status);

		progressDialog = new ProgressDialog(this);
		Intent mainIntent = getIntent();
		TextView tv4 = (TextView) findViewById(R.id.textView1);
		tv4.setText(mainIntent.getStringExtra("transStatus"));

		Log.d("paymeny result", mainIntent.getStringExtra("transStatus"));

		if(tv4.getText().toString().equals("Transaction Successful!")){
			tv4.setTextColor(Color.parseColor("#6ed64f"));
			setSubscription();
		} else{
			tv4.setTextColor(Color.parseColor("#d64f4f"));
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent intent = new Intent(StatusActivity.this, MainActivity.class);
					startActivity(intent);
					Toast.makeText(StatusActivity.this,"Thank you for subscribing with us",Toast.LENGTH_LONG).show();
					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);
		}


	}

	public void showToast(String msg) {
		Toast.makeText(this, "Toast: " + msg, Toast.LENGTH_LONG).show();
	}

	private void setSubscription() {
		progressDialog.setMessage("Redirecting...");
		progressDialog.show();
		progressDialog.setCancelable(false);
		AsyncHttpClient client = new AsyncHttpClient();
		final int DEFAULT_TIMEOUT = 20 * 10000;
		RequestParams params = new RequestParams();
		client.setTimeout(DEFAULT_TIMEOUT);
		client.post("http://www.chinmayaudghosh.com/api/user_subscription.php?sub_type="+getIntent().getStringExtra("subscription_type")+"&user_id="+ Utils.getFromUserDefaults(StatusActivity.this, Constant.PARAM_VALID_ID)+"&duration="+getIntent().getStringExtra("subscription_duration")+"&order_id="+getIntent().getStringExtra("subscription_orderid"), params, new GetResponsehandler());
	}

	public class GetResponsehandler extends AsyncHttpResponseHandler {

		@Override
		public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
			progressDialog.dismiss();
			Utils.saveToUserDefaults(StatusActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE, getIntent().getStringExtra("subscription_type"));
			Utils.saveToUserDefaults(StatusActivity.this, Constant.PARAM_VALID_SUBSCRIPTION, "1");
			Utils.saveToUserDefaults(StatusActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONDAYS,getIntent().getStringExtra("subscription_duration"));

			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent intent = new Intent(StatusActivity.this, MainActivity.class);
					startActivity(intent);
					Toast.makeText(StatusActivity.this,"Thank you for subscribing with us",Toast.LENGTH_LONG).show();
					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);

		}

		@Override
		public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
			progressDialog.dismiss();
			Toast.makeText(StatusActivity.this,"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
		}
	}
} 