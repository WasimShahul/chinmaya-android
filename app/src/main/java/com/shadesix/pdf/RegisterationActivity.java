package com.shadesix.pdf;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.models.RegistrationModel5;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.shadesix.pdf.utils.AppLocationService;
import com.shadesix.pdf.utils.GPSTracker;
import com.shadesix.pdf.utils.LocationAddress;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterationActivity extends AppCompatActivity {

    private static final int INITIAL_REQUEST = 1337;
    EditText fname, lname, email, age, state, city, pass, confirm, pincode;
    RadioGroup gender;
    TextView dob;
    Spinner country;
    Button register;
//    Spinner country;
    String subscribe_plan, fromAct;
    public String radioText = "";
    public String message;
    public int day, month, year;
    ProgressDialog progressDialog;
    LinearLayout ll_reg_country, lldob;
    static String selectedCountry = "India";
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    static String myAddr,myState,myCity,myPin;
    public static double lat_reg,long_reg;
    AppLocationService appLocationService;
    private static int countTimes = 0;

    private static int DELAY = 1000;
    private final Handler handler = new Handler();
    private final Timer timer = new Timer();
    private static boolean alertshowing = false;
    private AgeCalculation age2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);


        appLocationService = new AppLocationService(
                RegisterationActivity.this);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        age2 = new AgeCalculation();
        progressDialog = new ProgressDialog(this);
        register = (Button) findViewById(R.id.btn_reg_reg);
        fname = (EditText) findViewById(R.id.edt_reg_first);
        lname = (EditText) findViewById(R.id.edt_reg_last);
        email = (EditText) findViewById(R.id.edt_reg_email);
        age = (EditText) findViewById(R.id.edt_reg_age);
        dob = (TextView) findViewById(R.id.edt_reg_dob);
        country = (Spinner) findViewById(R.id.edt_reg_country);
        state = (EditText) findViewById(R.id.edt_reg_state);
        city = (EditText) findViewById(R.id.edt_reg_city);
        pass = (EditText) findViewById(R.id.edt_reg_password);
        confirm = (EditText) findViewById(R.id.edt_reg_confirm);
        pincode = (EditText) findViewById(R.id.edt_reg_pincode);
        gender = (RadioGroup) findViewById(R.id.gender_grp);
        ll_reg_country = (LinearLayout) findViewById(R.id.ll_reg_country);
        lldob = (LinearLayout) findViewById(R.id.lldob);

        subscribe_plan = getIntent().getStringExtra("plan");
        fromAct = getIntent().getStringExtra("from_act");

        lldob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectDate();
            }
        });

        alert();

        timer.schedule(task, 1000, DELAY);

        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    selectedCountry = "";
                } else{
                    selectedCountry = parent.getItemAtPosition(position).toString();
                    Log.d("RegistrationActivity",selectedCountry);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCountry = "";
            }
        });

//        pass.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String passStr = pass.getText().toString();
//                Pattern p = Pattern.compile("[^A-Za-z0-9@$&/\\*!_]");
//                Matcher m = p.matcher(passStr);
//
////                boolean b = m.find();
//                boolean b = false;
//
//                if (b || passStr.length()< 8){
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
//                    ViewCompat.setBackgroundTintList(pass, colorStateList);
//                }else if(passStr.length() >= 8){
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
//                    ViewCompat.setBackgroundTintList(pass, colorStateList);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        fname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = fname.getText().toString();
                Pattern p = Pattern.compile("^[\\p{L} .'-]+$]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(fname, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(fname, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {




            }
        });

        lname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = lname.getText().toString();
                Pattern p = Pattern.compile("^[\\p{L} .'-]+$]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(lname, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(lname, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        confirm.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String passStr = confirm.getText().toString();
//                Pattern p = Pattern.compile("[^A-Za-z0-9@$&/\\*!_]");
//                Matcher m = p.matcher(passStr);
//
//                boolean b = false;
//
//                if (b == true && !passStr.equals(pass.getText().toString())){
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
//                    ViewCompat.setBackgroundTintList(confirm, colorStateList);
//                }else{
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
//                    ViewCompat.setBackgroundTintList(confirm, colorStateList);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

        age.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = age.getText().toString();
                Pattern p = Pattern.compile("[^0-9]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(age, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(age, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = state.getText().toString();
                Pattern p = Pattern.compile("[^a-zA-Z\\s:]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(state, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(state, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = city.getText().toString();
                Pattern p = Pattern.compile("[^a-zA-Z\\s:]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(city, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(city, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        pincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String passStr = pincode.getText().toString();
                Pattern p = Pattern.compile("[^0-9]");
                Matcher m = p.matcher(passStr);

                boolean b = m.find();

                if (b == true){
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
                    ViewCompat.setBackgroundTintList(pincode, colorStateList);
                }else{
                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
                    ViewCompat.setBackgroundTintList(pincode, colorStateList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        email.setText(getIntent().getStringExtra("email"));

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validate()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(RegisterationActivity.this);
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    registerUser();
                    try {
                        Intent intent = new Intent(RegisterationActivity.this, com.shadesix.pdf.LoginActivity.class);
                        startActivity(intent);
                    } catch (Exception e) {

                    }
                }

            }
        });
    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        showDialog(1);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                DatePickerDialog dialog = new DatePickerDialog(this, pickerListener, year, month, day);
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                return dialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // Show selected date
            dob.setText(new StringBuilder().append(year)
                    .append("-").append(month + 1).append("-").append(day)
                    .append(""));
//            getAge(year,month+1,day);
        }
    };

    public void registerUser() {

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();

        params.add("country",selectedCountry.trim());
        params.add("fname",fname.getText().toString().trim());
        params.add("lname",lname.getText().toString().trim());
        params.add("password",pass.getText().toString());
        params.add("city",city.getText().toString().trim());
        params.add("dob",dob.getText().toString());
        params.add("state",state.getText().toString().trim());
        params.add("age",age.getText().toString().trim());
        params.add("email",email.getText().toString());
        params.add("cpassword",confirm.getText().toString());
        params.add("pincode",pincode.getText().toString());
        params.add("gender",radioText);
        client.setTimeout(DEFAULT_TIMEOUT);
        try {
            String url = "http://www.chinmayaudghosh.com/api/user_reg.php";
            url = url.replaceAll(" ","%20");
            client.post(url , params, new GetResponsehandler());
            Log.d("Registeration","http://www.chinmayaudghosh.com/api/user_reg.php?country=" + selectedCountry.trim() + "&fname=" + fname.getText().toString().trim() + "&lname=" + lname.getText().toString().trim() + "&password=" + URLEncoder.encode(pass.getText().toString(),"UTF-8") + "&city=" + city.getText().toString().trim() + "&dob=" + dob.getText().toString() + "&state=" + state.getText().toString().trim() + "&age=" + "" + "&email=" + email.getText().toString() + "&cpassword=" + URLEncoder.encode(confirm.getText().toString(),"UTF-8") + "&pincode=" + pincode.getText().toString() + "&gender=" + radioText);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("RegistrationActivity", "" + radioText);
    }


    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            RegistrationModel5 model = new Gson().fromJson(new String(responseBody), RegistrationModel5.class);
            if (model.success == 1) {
                Log.e("RegistrationActivity", "" + model.message);
                Toast.makeText(getApplicationContext(), model.message, Toast.LENGTH_LONG).show();
                try {

                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "Make sure you selected radio buttons", Toast.LENGTH_LONG).show();
                }
            } else {
                Log.e("RegistrationActivity", "" + model.message);
                Toast.makeText(getApplicationContext(), model.message, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(), "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    public boolean validate() {
        boolean isValidate = false;
        if (fname.getText().toString().trim().isEmpty() || lname.getText().toString().trim().isEmpty() || email.getText().toString().trim().isEmpty() ||
                selectedCountry.trim().isEmpty() || pass.getText().toString().trim().isEmpty() || confirm.getText().toString().trim().isEmpty() ) {
            isValidate = false;
            message = "Please enter all the fields";
        } else if (!validateEmail(email.getText().toString())) {
            isValidate = false;
            message = "Please enter a valid email id";
        } else if(!validatePassword(pass.getText().toString())){
            isValidate = false;
            message = "Please enter a valid Password";
        } else if(!validateDOB(dob.getText().toString())){
            isValidate = false;
            message = "Please enter your Date of birth";
        } else if(!validateName(fname.getText().toString())){
            isValidate = false;
            message = "Please enter a valid first name";
        } else if(!validateName(lname.getText().toString())){
            isValidate = false;
            message = "Please enter a valid last name";
        } else if(validateState(state.getText().toString())){
            isValidate = false;
            message = "Please enter a valid state";
        } else if(validateState(city.getText().toString())){
            isValidate = false;
            message = "Please enter a valid city";
        } else if (pass.getText().toString().length() < 8) {
            isValidate = false;
            message = "Password must be minimum of 8 characters";
        } else if (!pass.getText().toString().equals(confirm.getText().toString())) {
            isValidate = false;
            message = "Passwords do not match";
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    public boolean validateEmail(String mailId) {
        String ePattern = "^[a-zA-Z0-9.!$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(mailId);
        return m.matches();
    }

    public boolean validatePassword(String password) {
        String ePattern = "^[A-Za-z0-9@$#&/\\*!_]+$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(password);
        return m.matches();
    }

    public boolean validateName(String name) {
        String ePattern = "^[\\p{L} .'-]+$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(name);
        return m.matches();
    }

    public boolean validateDOB(String name) {
        if(name.trim().isEmpty()){
            return false;
        } else {
            return true;
        }
    }

    public boolean validateState(String name) {
        String ePattern = "[^\\w\\*]";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(name);
        return m.matches();
    }

    public boolean validatePincode(String name) {
        String ePattern = "^[a-zA-Z]+$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(name);
        return m.matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(RegisterationActivity.this, com.shadesix.pdf.LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.male_radio:
                if (checked)
                    radioText = "male";
                break;
            case R.id.female_radio:
                if (checked)
                    radioText = "female";
                break;
        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress = "", country2 = "", state3 = "", city2 = "", pincode2 = "";
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    country2 = bundle.getString("country");
                    state3 = bundle.getString("state");
                    city2 = bundle.getString("city");
                    pincode2 = bundle.getString("pincode");
                    break;
                default:
                    locationAddress = null;

            }
            if(countTimes <2){
                Log.e("registerActivity","times: "+countTimes);
                state.setText(state3);
                city.setText(city2);
                pincode.setText(pincode2);
//            country.setText(country2);
                countTimes = ++countTimes;
            }

        }
    }

    void alert(){

        final String[] mobileArray = {"Please select your country",
                "Afghanistan",
                "Albania",
                "Algeria",
                "Andorra",
                "Angola",
                "Antigua and Barbuda",
                "Argentina",
                "Armenia",
                "Australia",
                "Austria",
                "Azerbaijan",
                "Bahamas",
                "Bahrain",
                "Bangladesh",
                "Barbados",
                "Belarus",
                "Belgium",
                "Belize",
                "Benin",
                "Bhutan",
                "Bolivia",
                "Bosnia and Herzegovina",
                "Botswana",
                "Brazil",
                "Brunei",
                "Bulgaria",
                "Burkina Faso",
                "Burundi",
                "Cabo Verde",
                "Cambodia",
                "Cameroon",
                "Canada",
                "Central African Republic (CAR)",
                "Chad",
                "Chile",
                "China",
                "Colombia",
                "Comoros",
                "Democratic Republic of the Congo",
                "Republic of the Congo",
                "Costa Rica",
                "Cote d'Ivoire",
                "Croatia",
                "Cuba",
                "Cyprus",
                "Czech Republic",
                "Denmark",
                "Djibouti",
                "Dominica",
                "Dominican Republic",
                "Ecuador",
                "Egypt",
                "El Salvador",
                "Equatorial Guinea",
                "Eritrea",
                "Estonia",
                "Ethiopia",
                "Fiji",
                "Finland",
                "France",
                "Gabon",
                "Gambia",
                "Georgia",
                "Germany",
                "Ghana",
                "Greece",
                "Grenada",
                "Guatemala",
                "Guinea",
                "Guinea-Bissau",
                "Guyana",
                "Haiti",
                "Honduras",
                "Hungary",
                "Iceland",
                "India",
                "Indonesia",
                "Iran",
                "Iraq",
                "Ireland",
                "Israel",
                "Italy",
                "Jamaica",
                "Japan",
                "Jordan",
                "Kazakhstan",
                "Kenya",
                "Kiribati",
                "Kosovo",
                "Kuwait",
                "Kyrgyzstan",
                "Laos",
                "Latvia",
                "Lebanon",
                "Lesotho",
                "Liberia",
                "Libya",
                "Liechtenstein",
                "Lithuania",
                "Luxembourg",
                "Macedonia (FYROM)",
                "Madagascar",
                "Malawi",
                "Malaysia",
                "Maldives",
                "Mali",
                "Malta",
                "Marshall Islands",
                "Mauritania",
                "Mauritius",
                "Mexico",
                "Micronesia",
                "Moldova",
                "Monaco",
                "Mongolia",
                "Montenegro",
                "Morocco",
                "Mozambique",
                "Myanmar (Burma)",
                "Namibia",
                "Nauru",
                "Nepal",
                "Netherlands",
                "New Zealand",
                "Nicaragua",
                "Niger",
                "Nigeria",
                "North Korea",
                "Norway",
                "Oman",
                "Pakistan",
                "Palau",
                "Palestine",
                "Panama",
                "Papua New Guinea",
                "Paraguay",
                "Peru",
                "Philippines",
                "Poland",
                "Portugal",
                "Qatar",
                "Romania",
                "Russia",
                "Rwanda",
                "Saint Kitts and Nevis",
                "Saint Lucia",
                "Saint Vincent and the Grenadines",
                "Samoa",
                "San Marino",
                "Sao Tome and Principe",
                "Saudi Arabia",
                "Senegal",
                "Serbia",
                "Seychelles",
                "Sierra Leone",
                "Singapore",
                "Slovakia",
                "Slovenia",
                "Solomon Islands",
                "Somalia",
                "South Africa",
                "South Korea",
                "South Sudan",
                "Spain",
                "Sri Lanka",
                "Sudan",
                "Suriname",
                "Swaziland",
                "Sweden",
                "Switzerland",
                "Syria",
                "Taiwan",
                "Tajikistan",
                "Tanzania",
                "Thailand",
                "Timor-Leste",
                "Togo",
                "Tonga",
                "Trinidad and Tobago",
                "Tunisia",
                "Turkey",
                "Turkmenistan",
                "Tuvalu",
                "Uganda",
                "Ukraine",
                "United Arab Emirates (UAE)",
                "United Kingdom (UK)",
                "United States of America (USA)",
                "Uruguay",
                "Uzbekistan",
                "Vanuatu",
                "Vatican City (Holy See)",
                "Venezuela",
                "Vietnam",
                "Yemen",
                "Zambia",
                "Zimbabwe"};

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mobileArray);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        country.setAdapter(dataAdapter);

    }

    private final TimerTask task = new TimerTask() {

        public void run() {
            handler.post(new Runnable() {
                public void run() {
                    gps = new GPSTracker(RegisterationActivity.this);
                    if(gps.canGetLocation()){

                        lat_reg = gps.getLatitude();
                        long_reg = gps.getLongitude();
                        geocoder = new Geocoder(RegisterationActivity.this, Locale.getDefault());
                        try {
                            Log.e("Loca","hello "+lat_reg);
                            LocationAddress locationAddress = new LocationAddress();
                            locationAddress.getAddressFromLocation(lat_reg, long_reg,
                                    getApplicationContext(), new GeocoderHandler());
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String cit = addresses.get(0).getLocality();
                            String stat = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String postalCode = addresses.get(0).getPostalCode();

                            if(countTimes <2){
                                Log.e("registerActivity","times: "+countTimes);
                                state.setText(stat);
                                city.setText(cit);
                                pincode.setText(postalCode);
                                countTimes = ++countTimes;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        if(checkAndRequestPermissions()){
                            if(!alertshowing){
                                gps.showSettingsAlert();
                                alertshowing = true;
                            }

                        }
                    }
                }
            });

        }
    };

    private  boolean checkAndRequestPermissions() {
        int locationpermission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int locationpermission2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (locationpermission2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),INITIAL_REQUEST);
            return false;
        }
        return true;
    }

//    private void getAge(int year, int month, int day){
//        Calendar dob = Calendar.getInstance();
//        Calendar today = Calendar.getInstance();
//
//        dob.set(year, month, day);
//
//        int age1 = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
//
//        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
//            age1--;
//        }
//
//        Integer ageInt = new Integer(age1);
//        String ageS = ageInt.toString();
//        age.setText(ageS);
//    }

//    public int getAge (



}
