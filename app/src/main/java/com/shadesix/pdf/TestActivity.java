package com.shadesix.pdf;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.shadesix.pdf.adapters.CoverFlowAdapter;

import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class TestActivity extends AppCompatActivity {

    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    private ArrayList<GameEntity> mData = new ArrayList<>(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_third);

        mData.add(new GameEntity(R.drawable.january, "Title1"));
        mData.add(new GameEntity(R.drawable.february, "Title2"));
        mData.add(new GameEntity(R.drawable.march, "Title3"));
        mData.add(new GameEntity(R.drawable.april, "Title4"));
        mData.add(new GameEntity(R.drawable.may, "Title5"));
        mData.add(new GameEntity(R.drawable.june, "Title6"));
        mData.add(new GameEntity(R.drawable.july, "Title6"));
        mData.add(new GameEntity(R.drawable.august, "Title6"));
        mData.add(new GameEntity(R.drawable.september, "Title6"));
        mData.add(new GameEntity(R.drawable.october, "Title6"));
        mData.add(new GameEntity(R.drawable.november, "Title6"));
        mData.add(new GameEntity(R.drawable.december, "Title6"));

        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(mAdapter);


    }


}