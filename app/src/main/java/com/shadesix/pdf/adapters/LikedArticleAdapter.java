package com.shadesix.pdf.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.ArticleDetailActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.models.LikedModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class LikedArticleAdapter extends RecyclerView.Adapter<LikedArticleAdapter.MyViewHolder> {

    private Context context;
    public static ArrayList<LikedModel> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView description;
        ImageView cover;
        LinearLayout rel;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.article_title);
            this.description = (TextView) itemView.findViewById(R.id.airtcle_desc);
            this.cover = (ImageView) itemView.findViewById(R.id.img_cover);
            this.rel = (LinearLayout) itemView.findViewById(R.id.rel);

        }
    }

    public LikedArticleAdapter(ArrayList<LikedModel> data, Context cont) {
        this.data = data;
        this.context = cont;
    }

    @Override
    public LikedArticleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.liked_article_list, parent, false);

        LikedArticleAdapter.MyViewHolder myViewHolder = new LikedArticleAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final LikedArticleAdapter.MyViewHolder holder, final int listPosition) {

        TextView title = holder.title;
        TextView description = holder.description;
        LinearLayout rel = holder.rel;

        Log.d("LikedActivitydebug","url is "+Constant.PARAM_VALID_BASE_URL + data.get(listPosition).cover_path+" Title is "+data.get(listPosition).title);

        Picasso.with(context)
                .load(Constant.PARAM_VALID_BASE_URL + data.get(listPosition).cover_path)
                .into(holder.cover);
        title.setText(data.get(listPosition).title);
        description.setText(data.get(listPosition).content);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("article_id",data.get(listPosition).id);
                intent.putExtra("num_likes",data.get(listPosition).article_likes);
                intent.putExtra("is_liked","1");
                intent.putExtra("para",data.get(listPosition).content);
                intent.putExtra("title",data.get(listPosition).title);
                intent.putExtra("author",data.get(listPosition).author_name);
                intent.putExtra("pub_date",data.get(listPosition).published_date);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }
}

