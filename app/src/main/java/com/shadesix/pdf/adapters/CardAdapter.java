package com.shadesix.pdf.adapters;

import android.support.v7.widget.CardView;

/**
 * Created by Shade Six on 10-04-2017.
 */

public interface CardAdapter {
    int MAX_ELEVATION_FACTOR = 8;

    float getBaseElevation();

    CardView getCardViewAt(int position);

    int getCount();
}
