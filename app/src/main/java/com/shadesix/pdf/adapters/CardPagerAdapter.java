package com.shadesix.pdf.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shadesix.pdf.GetAllArticleListActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shade Six on 10-04-2017.
 */

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    static String TAG = CardPagerAdapter.class.getSimpleName();
    private List<CardView> mViews;
    private List<CardItem> mData;
    private float mBaseElevation;
    Context context;


    public CardPagerAdapter(Context mCont) {
        context = mCont;
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CardItem item) {
        Log.e(TAG,"add card item called ");
        mViews.add(null);
        mData.add(item);

    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Log.e(TAG,"instantiate called "+position);
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.adapter, container, false);
        container.addView(view);

        bind(mData.get(position), view,position);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent readMore = new Intent(context, GetAllArticleListActivity.class);
                readMore.putExtra("magazine_id",position+1);
                v.getContext().startActivity(readMore);
            }
        });

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(CardItem item, View view,int post) {
        ImageView cover = (ImageView) view.findViewById(R.id.img_magazine);
        TextView pubDate = (TextView) view.findViewById(R.id.input_date);
        TextView volume = (TextView) view.findViewById(R.id.input_vol);
        TextView issue = (TextView) view.findViewById(R.id.input_issue);
//        TextView titleTextView = (TextView) view.findViewById(R.id.titleTextView);
//        TextView contentTextView = (TextView) view.findViewById(R.id.contentTextView);

        Picasso.with(context)
                .load(Constant.PARAM_VALID_BASE_URL + item.getCoverPath())
                .into(cover);

        pubDate.setText(item.getPubDate());
        volume.setText(item.getVolume());
        issue.setText(item.getIssue());
    }

}
