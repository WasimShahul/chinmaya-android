package com.shadesix.pdf.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shadesix.pdf.R;
import com.shadesix.pdf.models.MagazineModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 13-04-2017.
 */

public class CoverFlowAdapterBackup extends BaseAdapter {

    private ArrayList<MagazineModel> mData = new ArrayList<>(0);
    private Context mContext;

    public CoverFlowAdapterBackup(Context context) {
        mContext = context;
    }

    public void setData(ArrayList<MagazineModel> data) {
        mData = data;
    }

    @Override
    public int getCount() {
        Log.d("coverflow", "Data size "+mData.size());
        return mData.size();
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_coverfolow, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.label);
            viewHolder.image = (ImageView) rowView
                    .findViewById(R.id.image);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

//        holder.image.setImagePath(mData.get(position).cover_path);
        Picasso.with(mContext)
//                .load(Constant.PARAM_VALID_BASE_URL+mData.get(position).cover_path)
                .load(R.drawable.compass)
                .into(holder.image);

//        holder.text.setText(mData.get(position).publishing_date);

        return rowView;
    }


    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }
}