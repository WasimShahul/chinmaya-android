package com.shadesix.pdf.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.DetailActivity;
import com.shadesix.pdf.LoginActivity;
import com.shadesix.pdf.MainActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.SubscriptionActivity;
import com.shadesix.pdf.activity.BillingShippingActivity;
import com.shadesix.pdf.activity.InitialScreenActivity;
import com.shadesix.pdf.activity.WebViewActivity;
import com.shadesix.pdf.models.GetSubscriptionModel;
import com.shadesix.pdf.models.GetSubscriptionModel2;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.utils.AvenuesParams;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.ServiceUtility;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 28-03-2017.
 */

public class GetSubscriptionAdapter extends RecyclerView.Adapter<GetSubscriptionAdapter.MyViewHolder> {

    private Context context;
    public String subscriptionType;
    public String subscriptionDuration;
    public String subscriptionAmount;
    public ArrayList<GetSubscriptionModel> data;
    public int row_index;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView cost;
        TextView subscription_Costinrduplicate;
        TextView costusd;
        TextView year_txt;
        LinearLayout relll;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.subscription_title);
            this.cost = (TextView) itemView.findViewById(R.id.subscription_Cost);
            this.subscription_Costinrduplicate = (TextView) itemView.findViewById(R.id.subscription_Costinrduplicate);
            this.costusd = (TextView) itemView.findViewById(R.id.subscription_Costusd);
            this.year_txt = (TextView) itemView.findViewById(R.id.year_txt);
            this.relll = (LinearLayout) itemView.findViewById(R.id.relll);

        }
    }

    public GetSubscriptionAdapter(ArrayList<GetSubscriptionModel> data, Context cont) {
        this.data = data;
        this.context = cont;
    }

    @Override
    public GetSubscriptionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_list_view, parent, false);

        GetSubscriptionAdapter.MyViewHolder myViewHolder = new GetSubscriptionAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final GetSubscriptionAdapter.MyViewHolder holder, final int listPosition) {

        final TextView title = holder.title;
        final TextView cost = holder.cost;
        final TextView subscription_Costinrduplicate = holder.subscription_Costinrduplicate;
        final TextView costusd = holder.costusd;
        final TextView year_txt = holder.year_txt;
        final LinearLayout rel = holder.relll;

        final SubscriptionActivity subscriptionActivity = new SubscriptionActivity();

        title.setText(data.get(listPosition).duration);
        cost.setText("INR: "+data.get(listPosition).price);
        costusd.setText("USD: "+data.get(listPosition).usd);

        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                row_index = listPosition;
                notifyDataSetChanged();
                title.setTextColor(Color.parseColor("#ffffff"));
                cost.setTextColor(Color.parseColor("#ffffff"));
                costusd.setTextColor(Color.parseColor("#ffffff"));
                year_txt.setTextColor(Color.parseColor("#ffffff"));
                rel.setBackgroundColor(Color.parseColor("#130A4B"));

                if(row_index==listPosition){
                    holder.relll.setBackgroundColor(Color.parseColor("#130A4B"));
                    holder.title.setTextColor(Color.parseColor("#ffffff"));
                    holder.cost.setTextColor(Color.parseColor("#ffffff"));
                    holder.costusd.setTextColor(Color.parseColor("#ffffff"));
                    holder.year_txt.setTextColor(Color.parseColor("#ffffff"));

                    subscriptionType = data.get(listPosition).id;
                    subscriptionDuration = data.get(listPosition).duration;
                    if(Utils.getFromUserDefaults(context, Constant.PARAM_VALID_COUNTRY).toLowerCase().trim().equals("india")){
                        String[] array = holder.cost.getText().toString().split(":");
                        subscriptionAmount = array[1].trim();
//                        Toast.makeText(context, subscriptionAmount + " : "+ subscriptionType+ " : "+ subscriptionDuration, Toast.LENGTH_SHORT).show();
                    } else{
                        String[] array = holder.costusd.getText().toString().split(":");
                        subscriptionAmount = array[1].trim();
//                    subscriptionAmount = holder.costusd.getText().toString();
//                    subscriptionAmount = data.get(listPosition).usd;
                    }

                }
                else
                {
                    holder.relll.setBackgroundColor(Color.parseColor("#ffffff"));
                    holder.title.setTextColor(Color.parseColor("#130A4B"));
                    holder.cost.setTextColor(Color.parseColor("#130A4B"));
                    holder.costusd.setTextColor(Color.parseColor("#130A4B"));
                    holder.year_txt.setTextColor(Color.parseColor("#130A4B"));
                }

                if(context instanceof SubscriptionActivity){
                    ((SubscriptionActivity)context).setbuttonvisibility();
                }
                if(context instanceof SubscriptionActivity){
//                    ((SubscriptionActivity)context).selectedSubsType(subscriptionType, subscriptionDuration, subscriptionAmount);
                    ((SubscriptionActivity)context).clickNextFunc(subscriptionType, subscriptionDuration, subscriptionAmount);
                }

            }
        });

        if(row_index==listPosition){
            holder.relll.setBackgroundColor(Color.parseColor("#130A4B"));
            holder.title.setTextColor(Color.parseColor("#ffffff"));
            holder.cost.setTextColor(Color.parseColor("#ffffff"));
            holder.costusd.setTextColor(Color.parseColor("#ffffff"));
            holder.year_txt.setTextColor(Color.parseColor("#ffffff"));

//            subscriptionType = data.get(listPosition).id;
//            subscriptionDuration = data.get(listPosition).duration;
//            if(Utils.getFromUserDefaults(context, Constant.PARAM_VALID_COUNTRY).toLowerCase().trim().equals("india")){
//                String[] array = holder.cost.getText().toString().split(":");
//                subscriptionAmount = array[1].trim();
//                Toast.makeText(context, subscriptionAmount, Toast.LENGTH_SHORT).show();
//            } else{
//                String[] array = holder.costusd.getText().toString().split(":");
//                subscriptionAmount = array[1].trim();
////                    subscriptionAmount = holder.costusd.getText().toString();
////                    subscriptionAmount = data.get(listPosition).usd;
//            }

        }
        else
        {
            holder.relll.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.title.setTextColor(Color.parseColor("#130A4B"));
            holder.cost.setTextColor(Color.parseColor("#130A4B"));
            holder.costusd.setTextColor(Color.parseColor("#130A4B"));
            holder.year_txt.setTextColor(Color.parseColor("#130A4B"));
        }

    }




    @Override
    public int getItemCount() {
        return data.size();
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }

    public void gotoDetail() {
    }

    public void moveToNextActivity(){
        String vAccessCode = "AVSN70EE54AD65NSDA";
        String vMerchantId = "132266";
        String vCurrency;
        if(Utils.getFromUserDefaults(context, Constant.PARAM_VALID_COUNTRY).toLowerCase().trim().equals("india")){
            vCurrency = "INR";
        } else {
            vCurrency = "USD";
        }

        String vAmount = subscriptionAmount;
        Integer randomNum = ServiceUtility.randInt(0, 9999999);
        if(!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")){
            Intent intent = new Intent(context,WebViewActivity.class);
            intent.putExtra(AvenuesParams.ACCESS_CODE, vAccessCode);
            intent.putExtra(AvenuesParams.MERCHANT_ID, vMerchantId);
            intent.putExtra(AvenuesParams.ORDER_ID, randomNum.toString());
            intent.putExtra(AvenuesParams.CURRENCY, vCurrency);
            intent.putExtra(AvenuesParams.AMOUNT, vAmount);
            intent.putExtra("subscription_type", subscriptionType);
            intent.putExtra("subscription_duration", subscriptionDuration);
            intent.putExtra("subscription_cost", subscriptionAmount);
            intent.putExtra("subscription_orderid", randomNum.toString());

            intent.putExtra(AvenuesParams.REDIRECT_URL, "http://www.chinmayaudghosh.com/api/ccavenue/ccavResponseHandler.php");
            intent.putExtra(AvenuesParams.CANCEL_URL, "http://www.chinmayaudghosh.com/api/ccavenue/ccavResponseHandler.php");
            intent.putExtra(AvenuesParams.RSA_KEY_URL, "http://www.chinmayaudghosh.com/api/ccavenue/GetRSA.php");

            context.startActivity(intent);
        }else{
            showToast("All parameters are mandatory.");
        }

    }

    public void showToast(String msg) {
        Toast.makeText(context, "Toast: " + msg, Toast.LENGTH_LONG).show();
    }
}
