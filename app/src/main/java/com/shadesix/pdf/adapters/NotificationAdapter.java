package com.shadesix.pdf.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shadesix.pdf.ArticleListActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.models.ExploreModel;
import com.shadesix.pdf.models.NotficationModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by shade6 on 01/05/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private Context context;
    public static ArrayList<NotficationModel> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView cover;
        RelativeLayout rel;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.txt_notify);
            this.cover = (ImageView) itemView.findViewById(R.id.img_notify_icon);
            this.rel = (RelativeLayout) itemView.findViewById(R.id.rel);

        }
    }

    public NotificationAdapter(ArrayList<NotficationModel> data, Context cont) {
        this.data = data;
        this.context = cont;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_view, parent, false);

        NotificationAdapter.MyViewHolder myViewHolder = new NotificationAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.MyViewHolder holder, final int listPosition) {

        TextView title = holder.title;
        RelativeLayout rel = holder.rel;

        if(data.get(listPosition).notify_type.equals("gen")){
            holder.cover.setImageResource(R.drawable.general);
            rel.setBackgroundColor(Color.parseColor("#130A4B"));
        }else if(data.get(listPosition).notify_type.equals("mag")){
            holder.cover.setImageResource(R.drawable.icon);
            rel.setBackgroundColor(Color.parseColor("#d77c3a"));
        }else {
            holder.cover.setImageResource(R.drawable.subscription);
            rel.setBackgroundColor(Color.parseColor("#130A4B"));
        }

        title.setText(data.get(listPosition).notify_text);
//        rel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(context, ArticleListActivity.class);
//                intent.putExtra("cover_path",data.get(listPosition).cover_path);
//                intent.putExtra("title",data.get(listPosition).title);
//                intent.putExtra("id",data.get(listPosition).id);
////                intent.putExtra("num_likes",data.get(listPosition).article_likes);
//                context.startActivity(intent);
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }
} 
