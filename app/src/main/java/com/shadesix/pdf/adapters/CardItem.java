package com.shadesix.pdf.adapters;

/**
 * Created by Shade Six on 10-04-2017.
 */

public class CardItem {
    private int mTextResource;
    private int mTitleResource;
    private String coverPath;
    private String pubDate;
    private String volume;
    private String issue;

    public CardItem(String coverPath, String pubDate, String volume, String issue) {
        this.pubDate = pubDate;
        this.volume = volume;
        this.issue = issue;
       this.coverPath = coverPath;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getVolume() {
        return volume;
    }

    public String getIssue() {
        return issue;
    }

}
