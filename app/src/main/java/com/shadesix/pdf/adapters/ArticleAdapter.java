package com.shadesix.pdf.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shadesix.pdf.ArticleDetailActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.models.GetArticleModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 27-03-2017.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    private Context context;
    private int count;
    private String count1;
    public static ArrayList<GetArticleModel> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView article_like;
        TextView description;
        ImageView cover;
        LinearLayout rel;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.article_title);
            this.article_like = (TextView) itemView.findViewById(R.id.article_like);
            this.description = (TextView) itemView.findViewById(R.id.airtcle_desc);
            this.cover = (ImageView) itemView.findViewById(R.id.img_cover);
            this.rel = (LinearLayout) itemView.findViewById(R.id.rel);

        }
    }

    public ArticleAdapter(ArrayList<GetArticleModel> data, Context cont, int count, String count1) {
        this.data = data;
        this.context = cont;
        this.count = count;
        this.count1 = count1;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_list_view, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView title = holder.title;
        final TextView article_like = holder.article_like;
        TextView description = holder.description;
        LinearLayout rel = holder.rel;

        final Typeface font = Typeface.createFromAsset(context.getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(context.getAssets(), "paitb0.ttf");

        Picasso.with(context)
                .load(Constant.PARAM_VALID_BASE_URL + data.get(listPosition).cover_path)
                .into(holder.cover);
        title.setText(data.get(listPosition).title);
        article_like.setText(data.get(listPosition).is_liked);
        description.setText(Html.fromHtml(data.get(listPosition).content));
        description.setTypeface(font);
        title.setTypeface(fontb);
        Log.e("Article Adapter",data.get(listPosition).cover_path);
        Log.e("Article Adapter",data.get(listPosition).content);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ArticleDetailActivity.class);
                intent.putExtra("article_id",data.get(listPosition).id);
                intent.putExtra("num_likes",data.get(listPosition).article_likes);
                intent.putExtra("is_liked",article_like.getText().toString());
                intent.putExtra("author",data.get(listPosition).author_name);
                intent.putExtra("title",data.get(listPosition).title);
                intent.putExtra("para",data.get(listPosition).content);
                intent.putExtra("count1",count1);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }
}
