package com.shadesix.pdf.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shadesix.pdf.ArticleListActivity;
import com.shadesix.pdf.DetailActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 28-03-2017.
 */

public class MagazineAdapter extends RecyclerView.Adapter<MagazineAdapter.MyViewHolder> {

    private Context context;
    public static ArrayList<MagazineModel> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView airtcle_desc;
        ImageView cover;
        LinearLayout rel;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.article_title);
            this.airtcle_desc = (TextView) itemView.findViewById(R.id.airtcle_desc);
            this.cover = (ImageView) itemView.findViewById(R.id.img_cover);
            this.rel = (LinearLayout) itemView.findViewById(R.id.rel);

        }
    }

    public MagazineAdapter(ArrayList<MagazineModel> data, Context cont) {
        this.data = data;
        this.context = cont;
    }

    @Override
    public MagazineAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_list_view, parent, false);

        MagazineAdapter.MyViewHolder myViewHolder = new MagazineAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MagazineAdapter.MyViewHolder holder, final int listPosition) {

        TextView title = holder.title;
        TextView airtcle_desc = holder.airtcle_desc;
        LinearLayout rel = holder.rel;

        final Typeface font = Typeface.createFromAsset(context.getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(context.getAssets(), "paitb0.ttf");
        Typeface fontbi = Typeface.createFromAsset(context.getAssets(), "paitbi0.ttf");
        Typeface fonti = Typeface.createFromAsset(context.getAssets(), "paiti0.ttf");

        title.setTypeface(fontb);
        airtcle_desc.setTypeface(font);

        Picasso.with(context)
                .load(Constant.PARAM_VALID_BASE_URL + data.get(listPosition).cover_path)
                .into(holder.cover);
        title.setText(data.get(listPosition).title);
        airtcle_desc.setText(data.get(listPosition).content);



        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Activity activity = (Activity) context;
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                        new Pair(holder.cover, DetailActivity.EXTRA_IMAGE_URL)
//                        new Pair(address1, DetailActivity.ADDRESS1_TRANSITION_NAME),
//                        new Pair(address3, DetailActivity.ADDRESS3_TRANSITION_NAME),
//                        new Pair(address4, DetailActivity.ADDRESS4_TRANSITION_NAME),
//                        new Pair(address5, DetailActivity.ADDRESS5_TRANSITION_NAME)
//                new Pair(ratingBar, DetailActivity.RATINGBAR_TRANSITION_NAME),
//                new Pair(head1, DetailActivity.HEAD1_TRANSITION_NAME),
//                new Pair(head2, DetailActivity.HEAD2_TRANSITION_NAME),
//                new Pair(head3, DetailActivity.HEAD3_TRANSITION_NAME),
//                new Pair(head4, DetailActivity.HEAD4_TRANSITION_NAME)
                );
                Intent intent = new Intent(context, DetailActivity.class);
//        intent.putExtra(DetailActivity.EXTRA_IMAGE_URL, imageUrl);
                intent.putExtra("magazine_id", data.get(listPosition).id);
                intent.putExtra("magazine_path", data.get(listPosition).pdf_path);
                intent.putExtra("pubDate",data.get(listPosition).publishing_date);
                intent.putExtra("magTitle",data.get(listPosition).title);
                intent.putExtra("MagVol",data.get(listPosition).volume);
                intent.putExtra("magIssue",data.get(listPosition).issue);
                intent.putExtra("magCover",data.get(listPosition).cover_path);
                ActivityCompat.startActivity(activity, intent, options.toBundle());

//                Intent intent = new Intent(context, ArticleListActivity.class);
//                intent.putExtra("cover_path",data.get(listPosition).cover_path);
//                intent.putExtra("title",data.get(listPosition).title);
//                intent.putExtra("id",data.get(listPosition).id);
////                intent.putExtra("num_likes",data.get(listPosition).article_likes);
//                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return 6;
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }

    public void gotoDetail() {

//        startActivity(intent);
    }
}
