package com.shadesix.pdf.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.shadesix.pdf.ArticleListActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.models.LatestArticleModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Shade Six on 11-04-2017.
 */

public class LatestArticleAdapter extends RecyclerView.Adapter<LatestArticleAdapter.MyViewHolder> {

    private Context context;
    public static ArrayList<LatestArticleModel> data;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView cover;
        FrameLayout rel;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.item_text);
            this.cover = (ImageView) itemView.findViewById(R.id.item_image);
            this.rel = (FrameLayout) itemView.findViewById(R.id.layout_explore);

        }
    }

    public LatestArticleAdapter(ArrayList<LatestArticleModel> data, Context cont) {
        this.data = data;
        this.context = cont;
    }

    @Override
    public LatestArticleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {


        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.explorer_list_view, parent, false);

        LatestArticleAdapter.MyViewHolder myViewHolder = new LatestArticleAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final LatestArticleAdapter.MyViewHolder holder, final int listPosition) {

        TextView title = holder.title;
        FrameLayout rel = holder.rel;

        Picasso.with(context)
                .load(Constant.PARAM_VALID_BASE_URL + data.get(listPosition).cover_path)
                .into(holder.cover);
        title.setText(data.get(listPosition).title);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ArticleListActivity.class);
                intent.putExtra("cover_path",data.get(listPosition).cover_path);
                intent.putExtra("title",data.get(listPosition).title);
                intent.putExtra("article_id",data.get(listPosition).id);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return 1;
    }


    public void clear() {
        data.clear();
        this.notifyDataSetChanged();
    }
}