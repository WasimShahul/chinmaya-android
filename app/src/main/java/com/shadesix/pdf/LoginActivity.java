package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.ForgotPassActivity;
import com.shadesix.pdf.models.LoginModel;
import com.shadesix.pdf.models.LoginModel2;
import com.shadesix.pdf.models.SubscribeDetailModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pl.droidsonroids.gif.GifTextView;

public class LoginActivity extends AppCompatActivity {

    static String TAG = LoginActivity.class.getSimpleName();
    EditText email,password;
    Button btnLogin,btnReg;
    String subscribe_plan,fromAct;
    ArrayList<LoginModel> userList;
    LinearLayout loginLL;
    GifTextView loadingGif;
    public String message;
    static Map<String,String> contactList = new HashMap<>();
    ProgressDialog progressDialog;
    TextView forgotPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        email = (EditText) findViewById(R.id.edt_login_email);
        loginLL = (LinearLayout) findViewById(R.id.loginLL);
        password = (EditText) findViewById(R.id.edt_login_password);
        forgotPass = (TextView) findViewById(R.id.txt_forgot);
        btnLogin = (Button) findViewById(R.id.btn_login_login);
        btnReg = (Button) findViewById(R.id.btn_login_reg);
        loadingGif = (GifTextView) findViewById(R.id.loadingGif);
        loadingGif.setVisibility(View.GONE);
        loginLL.setVisibility(View.VISIBLE);

        subscribe_plan = getIntent().getStringExtra("plan");
        fromAct = getIntent().getStringExtra("from_act");

        userList = new ArrayList<LoginModel>();

        try {
            if(!Utils.getFromUserDefaults(LoginActivity.this, Constant.PARAM_VALID_EMAIL).isEmpty()){
                Intent intent = new Intent(LoginActivity.this,PaymentGatewayActivity.class);
                intent.putExtra("plan",subscribe_plan);
                intent.putExtra("mail",Utils.getFromUserDefaults(LoginActivity.this, Constant.PARAM_VALID_EMAIL));
                startActivity(intent);
                finish();
            }
        }catch (Exception ex){

        }

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassActivity.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validate()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    sendToServer();
                }
            }
        });


        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    startActivity(new Intent(LoginActivity.this, RegisterationActivity.class));
                    finish();
                } catch (Exception e){

                }

            }
        });
    }

    private void sendToServer(){
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("username", email.getText().toString());
        params.put("password", password.getText().toString());
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/user_login.php", params, new LoginActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            LoginModel2 model = new Gson().fromJson(new String(responseBody), LoginModel2.class);
            progressDialog.dismiss();
            if (model.success == 1) {
                try {
                    for (LoginModel mydealmodel : model.userDetail) {
                        userList.add(mydealmodel);
                        Log.e("Email onSuccess", mydealmodel.email);
                    }
                    for (int i = 0; i < userList.size(); i++) {
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_FIRSTNAME, userList.get(i).first_name);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_LASTNAME, userList.get(i).last_name);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_EMAIL, userList.get(i).email);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_DOB, userList.get(i).date_of_birth);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_COUNTRY, userList.get(i).country);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_STATE, userList.get(i).state);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_CITY, userList.get(i).city);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_ID, userList.get(i).id);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_GENDER, userList.get(i).gender);
                        Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_AGE, userList.get(i).age);
                    }
                    Log.e(TAG, "Valid email " + Utils.getFromUserDefaults(LoginActivity.this, Constant.PARAM_VALID_EMAIL));
                    try{
                        if(fromAct.equals("ArticleDetail")){
                            Log.e("Loginactivity", "articleDetail");
                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            getSubscriptionDetailInitially();
                            finish();
                        }else if(fromAct.equals("Subscription")) {
                            Log.e("Loginactivity", "Subscription");
                            if(Utils.getFromUserDefaults(LoginActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
                                startActivity(new Intent(LoginActivity.this,SubscriptionActivity.class));
                                getSubscriptionDetailInitially();
                                finish();
                            }else {
                                Log.e(TAG,"ShowSubscription");
                                getSubscriptionDetail();

                            }
                            finish();
                        }else if(fromAct.equals("main")) {
                            Log.e("Loginactivity", "main");
                            getSubscriptionDetailInitially();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Log.e("Loginactivity", "nothig");
                            getSubscriptionDetailInitially();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } catch (Exception e){
                        getSubscriptionDetailInitially();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }catch (Exception e){

                }
            }
            else{
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Invalid username or password",Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            try{
                if(fromAct.equals("ArticleDetail")){
                    Log.e("Loginactivity", "articleDetail");
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    finish();
                }else if(fromAct.equals("Subscription")) {
                    Log.e("Loginactivity", "Subscription");
                    if(Utils.getFromUserDefaults(LoginActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
                        startActivity(new Intent(LoginActivity.this,SubscriptionActivity.class));
                        finish();
                    }else {
                        Log.e(TAG,"ShowSubscription");
                        getSubscriptionDetail();

                    }
                    finish();
                }else if(fromAct.equals("main")) {
                    Log.e("Loginactivity", "main");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Log.e("Loginactivity", "nothig");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            } catch (Exception e){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        boolean isValidate = false;
        if (!validateEmail(email.getText().toString())) {
            isValidate = false;
            message = "Please enter a valid email id";
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    public boolean validateEmail(String mailId) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(mailId);
        return m.matches();
    }

    private void getSubscriptionDetail(){
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
//        Toast.makeText(getBaseContext(),"Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(LoginActivity.this,Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_user_subscription.php", params, new GetResponsehandler1());
    }

    public class GetResponsehandler1 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            SubscribeDetailModel2 model = new Gson().fromJson(new String(responseBody), SubscribeDetailModel2.class);
            if (model.success == 1) {
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
            else if(model.success == 2){
//                Toast.makeText(getApplicationContext(),"subs "+model.type,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(LoginActivity.this,ShowUserSubscriptionActivity.class);
                intent.putExtra("type",model.type);
                intent.putExtra("remaining",model.remaining);
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE,model.type);
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTION,"1");
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONDAYS,model.remaining);
                startActivity(intent);
            }else {
                startActivity(new Intent(LoginActivity.this,SubscriptionActivity.class));
                finish();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    private void getSubscriptionDetailInitially(){
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(LoginActivity.this,Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_user_subscription.php", params, new GetResponsehandler2());
    }

    public class GetResponsehandler2 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            SubscribeDetailModel2 model = new Gson().fromJson(new String(responseBody), SubscribeDetailModel2.class);
            if (model.success == 1) {
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
            else if(model.success == 2){
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE,model.type);
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTION,"1");
                Utils.saveToUserDefaults(LoginActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONDAYS,model.remaining);
            }else {
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try{
            if(fromAct.equals("ArticleDetail")){
                Log.e("Loginactivity", "articleDetail");
                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                finish();
            }else if(fromAct.equals("Subscription")) {
                Log.e("Loginactivity", "Subscription");
                if(Utils.getFromUserDefaults(LoginActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
                    startActivity(new Intent(LoginActivity.this,SubscriptionActivity.class));
                    finish();
                }else {
                    Log.e(TAG,"ShowSubscription");
                    getSubscriptionDetail();

                }
                finish();
            }else if(fromAct.equals("main")) {
                Log.e("Loginactivity", "main");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Log.e("Loginactivity", "nothig");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (Exception e){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
