package com.shadesix.pdf;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.adapters.ArticleAdapter;
import com.shadesix.pdf.models.GetArticleModel;
import com.shadesix.pdf.models.GetArticleModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    public static ArrayList<GetArticleModel> artList = new ArrayList<GetArticleModel>();
    ArticleAdapter dealadapter;

    public static final String EXTRA_IMAGE_URL = "http://www.chinmayaudghosh.com/magazineimage/april.png";

    public static final String IMAGE_TRANSITION_NAME = "transitionImage";
    public static final String ADDRESS1_TRANSITION_NAME = "address1";
    public static final String ADDRESS3_TRANSITION_NAME = "address3";
    public static final String ADDRESS4_TRANSITION_NAME = "address4";
    public static final String ADDRESS5_TRANSITION_NAME = "address5";

    private TextView address1, address3, address4, address5;
    private ImageView imageView, img_back,img_small_view;

    private int count;
    public String count1;

    private RecyclerView listContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        final Typeface font = Typeface.createFromAsset(getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(getAssets(), "paitb0.ttf");
        String magazine_id = getIntent().getStringExtra("magazine_id");
        String magPubDate = getIntent().getStringExtra("pubDate");
        String magIssue = getIntent().getStringExtra("magIssue");
        String magVol = getIntent().getStringExtra("MagVol");
        String magCover = getIntent().getStringExtra("magCover");
        String magTitle = getIntent().getStringExtra("magTitle");
        count1 = getIntent().getStringExtra("count1");
        final String magPath = getIntent().getStringExtra("magazine_path");

        Log.d("detailactivity","magid is "+magazine_id);


        imageView = (ImageView) findViewById(R.id.image);
        img_small_view = (ImageView) findViewById(R.id.img_book_view);
        address1 = (TextView) findViewById(R.id.address1);
        address3 = (TextView)findViewById(R.id.address3);
        address4 = (TextView)findViewById(R.id.address4);
        address5 = (TextView)findViewById(R.id.address5);

        listContainer = (RecyclerView) findViewById(R.id.detail_list_container);

        address1.setTypeface(font);
        address3.setTypeface(font);
        address4.setTypeface(fontb);
        address5.setTypeface(font);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.getFromUserDefaults(DetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).isEmpty() || Utils.getFromUserDefaults(DetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).equals("0")){
                    try{
                        if(count1.equals("12")){
                            Intent intent = new Intent(DetailActivity.this, com.shadesix.pdf.PdfViewerActivityWasim.class);
                            intent.putExtra("magazine_path", magPath);
                            startActivity(intent);
                        } else{
                            Toast.makeText(DetailActivity.this,"Kindly subscribe to view the magazine",Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(DetailActivity.this,"Kindly subscribe to view the magazine",Toast.LENGTH_LONG).show();
                    }
                } else{
                    Intent intent = new Intent(DetailActivity.this, com.shadesix.pdf.PdfViewerActivityWasim.class);
                    intent.putExtra("magazine_path", magPath);
                    startActivity(intent);
                }

            }
        });

        img_small_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.getFromUserDefaults(DetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).isEmpty() || Utils.getFromUserDefaults(DetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).equals("0")){
                    try{
                        if(count1.equals("12")){
                            Intent intent = new Intent(DetailActivity.this, com.shadesix.pdf.PdfViewerActivityWasim.class);
                            intent.putExtra("magazine_path", magPath);
                            startActivity(intent);
                        } else{
                            Toast.makeText(DetailActivity.this,"Kindly subscribe to view the magazine",Toast.LENGTH_LONG).show();
                        }
                    }catch (Exception e){
                        Toast.makeText(DetailActivity.this,"Kindly subscribe to view the magazine",Toast.LENGTH_LONG).show();
                    }
                } else{
                    Intent intent = new Intent(DetailActivity.this, com.shadesix.pdf.PdfViewerActivityWasim.class);
                    intent.putExtra("magazine_path", magPath);
                    startActivity(intent);
                }
            }
        });

        Picasso.with(DetailActivity.this).load(Constant.PARAM_VALID_BASE_URL+magCover).into(imageView);
        Log.d("detailactivity","url is "+ Constant.PARAM_VALID_BASE_URL+magCover);
        ViewCompat.setTransitionName(imageView, IMAGE_TRANSITION_NAME);
        ViewCompat.setTransitionName(address1, ADDRESS1_TRANSITION_NAME);
        ViewCompat.setTransitionName(address3, ADDRESS3_TRANSITION_NAME);
        ViewCompat.setTransitionName(address4, ADDRESS4_TRANSITION_NAME);
        ViewCompat.setTransitionName(address5, ADDRESS5_TRANSITION_NAME);
        address4.setText(magTitle);
        address1.setText("Issue: "+magIssue);
        try{
            if(count1.equals("12")){
                address3.setText("Volume: "+magVol+"\n"+"Free Issue");
            } else{
                address3.setText("Volume: "+magVol);
            }
        } catch(Exception e){
            address3.setText("Volume: "+magVol);
        }

        address5.setText(magPubDate);
        getArticles();

    }



    public void getArticles(){
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_articles_by_magazine.php?magazine_id="+getIntent().getStringExtra("magazine_id"), params, new DetailActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            GetArticleModel2 model = new Gson().fromJson(new String(responseBody), GetArticleModel2.class);
            artList.clear();
            if (model.success == 1) {
                try {
                    Log.e("detailactivity","2");
                    for (GetArticleModel gam : model.articleDetail) {
                        artList.add(gam);
                        dealadapter = new ArticleAdapter(artList, DetailActivity.this, count,count1);
                        count = ++count;
                        Log.e("detailactivity",gam.cover_path);
                        Log.e("detailactivity",gam.content);
                        Log.e("detailactivity",gam.created_time);
                        Log.e("detailactivity",gam.article_likes);
                        Log.e("detailactivity",gam.author_name);
                    }
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(DetailActivity.this, 1);
                    listContainer.setLayoutManager(mLayoutManager);
                    listContainer.setAdapter(dealadapter);

                }catch (Exception e){
                    Log.e("detailactivity",""+e.getMessage());
                }
            }
            else{
                Toast.makeText(DetailActivity.this,model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(DetailActivity.this,"Unable to get article title list.", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
