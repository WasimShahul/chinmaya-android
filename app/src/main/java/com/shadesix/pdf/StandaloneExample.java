package com.shadesix.pdf;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.widget.Toast;

import com.shadesix.pdf.adapters.PageCurlView;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.models.MagazineModel2;
import com.shadesix.pdf.utils.Constant;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.net.URL;
import java.util.ArrayList;

public class StandaloneExample extends Activity {

    ArrayList<MagazineModel> userDetail = new ArrayList<>();
    PageCurlView pageCurlView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_standalone_example);

        pageCurlView = (PageCurlView) findViewById(R.id.dcgpagecurlPageCurlView1);

        getPdfPath();
    }

    private void getPdfPath(){

//        Toast.makeText(getBaseContext(),"Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("magazine_id","3");
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_magazines.php", params, new StandaloneExample.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            MagazineModel2 model = new Gson().fromJson(new String(responseBody), MagazineModel2.class);
            if (model.success == 1) {
                try {
                    for (MagazineModel mydealmodel : model.magazineDetail) {
                        userDetail.add(mydealmodel);
                    }
                    for(int i=0;i<userDetail.size();i++){
                        URL url = new URL(Constant.PARAM_VALID_BASE_URL + userDetail.get(i).pdf_path);
                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        BitmapDrawable background = new BitmapDrawable(bmp);
                        pageCurlView.setBackgroundDrawable(background);
                    }

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        System.gc();
        finish();
    }

    /**
     * Set the current orientation to landscape. This will prevent the OS from changing
     * the app's orientation.
     */
    public void lockOrientationLandscape() {
        lockOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    /**
     * Set the current orientation to portrait. This will prevent the OS from changing
     * the app's orientation.
     */
    public void lockOrientationPortrait() {
        lockOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * Locks the orientation to a specific type.  Possible values are:
     * <ul>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_BEHIND}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_LANDSCAPE}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_NOSENSOR}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_PORTRAIT}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_SENSOR}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_UNSPECIFIED}</li>
     * <li>{@link ActivityInfo#SCREEN_ORIENTATION_USER}</li>
     * </ul>
     * @param orientation
     */
    public void lockOrientation( int orientation ) {
        setRequestedOrientation(orientation);
    }
}
