package com.shadesix.pdf;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.adapters.ArticleAdapter;
import com.shadesix.pdf.models.GetArticleModel;
import com.shadesix.pdf.models.GetArticleModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArticleListActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private AppBarLayout appBarLayout = null;
    TextView name,number;
    ImageView articleCover;
    private int count;
    RelativeLayout rel;
    RecyclerView articleList;
    ArticleAdapter dealadapter;
    public static ArrayList<GetArticleModel> prevList = new ArrayList<GetArticleModel>();
    public String coverPath,title,article_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        name = (TextView) findViewById(R.id.txt_name);
        number = (TextView) findViewById(R.id.txt_article_num);
        rel = (RelativeLayout) findViewById(R.id.rel_article);
        articleCover = (ImageView) findViewById(R.id.profile_id);
        articleList = (RecyclerView) findViewById(R.id.article_list);

        coverPath = getIntent().getStringExtra("cover_path");
        title = getIntent().getStringExtra("title");
        article_id = getIntent().getStringExtra("id");

        Picasso.with(ArticleListActivity.this)
                .load(Constant.PARAM_VALID_BASE_URL + coverPath)
                .into(articleCover);

        name.setText(title);

        getArticleList();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(verticalOffset == 0){
                    collapsingToolbarLayout.setTitle("");
                    rel.setVisibility(View.VISIBLE);
                }else {
                    rel.setVisibility(View.GONE);
                    collapsingToolbarLayout.setTitle(title);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            startActivity(new Intent(ArticleListActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getArticleList(){
//        Toast.makeText(ArticleListActivity.this, "Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        if(!Utils.getFromUserDefaults(ArticleListActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
            params.put("user_id",Utils.getFromUserDefaults(ArticleListActivity.this,Constant.PARAM_VALID_ID));
        }
        client.setTimeout(DEFAULT_TIMEOUT);
        params.put("article_id",article_id);
        client.get("http://www.chinmayaudghosh.com/api/get_all_article_editions.php", params, new ArticleListActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            GetArticleModel2 model = new Gson().fromJson(new String(responseBody), GetArticleModel2.class);
            if (model.success == 1) {
                prevList.clear();
                try {
                    for (GetArticleModel mydealmodel : model.articleDetail) {
                        prevList.add(mydealmodel);
                        dealadapter = new ArticleAdapter(prevList, ArticleListActivity.this, count, "");
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ArticleListActivity.this, 1);
                        articleList.setLayoutManager(mLayoutManager);
                        articleList.setAdapter(dealadapter);
                        count = ++count;
                    }
                    number.setText(""+prevList.size());

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }
}
