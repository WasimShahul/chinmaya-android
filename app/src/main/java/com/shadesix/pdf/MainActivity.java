package com.shadesix.pdf;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.fragments.CarouselFragment;
import com.shadesix.pdf.fragments.CompassFragment;
import com.shadesix.pdf.fragments.HomeFragment;
import com.shadesix.pdf.models.SubscribeDetailModel;
import com.shadesix.pdf.models.SubscribeDetailModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    static String TAG = MainActivity.class.getSimpleName();
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ArrayList<SubscribeDetailModel> userList;
    private int[] tabIcons = {
            R.drawable.home,
            R.drawable.compass,
            R.drawable.magazine
    };

    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

//        sendNotification("title", "message");

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        userList = new ArrayList<>();

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.tabSelectedIconColor);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.tabUnselectedIconColor);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
        setupTabIcons();

        int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.tabSelectedIconColor);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        TextView myName = (TextView) header.findViewById(R.id.txt_fullname);
        final TextView loginout = (TextView) header.findViewById(R.id.txt_logout);
//        RelativeLayout explore_rl = (RelativeLayout) header.findViewById(R.id.explore_rel);
        RelativeLayout notification_rl = (RelativeLayout) header.findViewById(R.id.notification_rel);
        RelativeLayout likes_rl = (RelativeLayout) header.findViewById(R.id.likes_rel);
        RelativeLayout logout_rl = (RelativeLayout) header.findViewById(R.id.logout_rel);
        RelativeLayout subscribe_rl = (RelativeLayout) header.findViewById(R.id.subscription_rel);
        RelativeLayout quotes_rl = (RelativeLayout) header.findViewById(R.id.quotes_rel);
        RelativeLayout contact_rl = (RelativeLayout) header.findViewById(R.id.contact_rel);


        if(Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
           loginout.setText("LOGIN");
        }else {
            loginout.setText("LOG OUT");
        }

        if(Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
            likes_rl.setVisibility(View.GONE);
        }

        if(!Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_FIRSTNAME).isEmpty()){
            myName.setText(Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_FIRSTNAME));
        }

        notification_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,NotificationActivity.class));
                finish();
            }
        });

        likes_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,LikedArticleListActivity.class));
            }
        });

        getSubscriptionDetailInitially();

        subscribe_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID));
                if(Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID).isEmpty()){
                    startActivity(new Intent(MainActivity.this,SubscriptionActivity.class));
                    finish();
                }else {
                    Log.e(TAG,"ShowSubscription");
                    getSubscriptionDetail();

                }

            }
        });

        quotes_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this,QuoteActivity.class));
                    finish();
            }
        });

        contact_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this,ContactActivity.class));
                    finish();
            }
        });

        logout_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginout.getText().toString().equals("LOG OUT")) {
                    showLogoutAlert();
                }else {
                    Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                    intent.putExtra("from_act","main");
                    startActivity(intent);
                    finish();
                }
            }
        });


        FirebaseMessaging.getInstance().subscribeToTopic("quotelydon");
//        FirebaseMessaging.getInstance().subscribeToTopic("test");

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        calendar.set(Calendar.HOUR_OF_DAY, 16);
        calendar.set(Calendar.MINUTE, 50);
        calendar.set(Calendar.SECOND, 0);


//        Intent myIntent = new Intent(MainActivity.this, MyReceiver.class);
//        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent,0);

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

    }

    private void getSubscriptionDetail(){
//        Toast.makeText(getBaseContext(),"Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_user_subscription.php", params, new MainActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            SubscribeDetailModel2 model = new Gson().fromJson(new String(responseBody), SubscribeDetailModel2.class);
            if (model.success == 1) {
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
            else if(model.success == 2){
//                Toast.makeText(getApplicationContext(),"subs "+model.type,Toast.LENGTH_LONG).show();
               Intent intent = new Intent(MainActivity.this,ShowUserSubscriptionActivity.class);
                intent.putExtra("type",model.type);
                intent.putExtra("remaining",model.remaining);
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE,model.type);
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTION,"1");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONDAYS,model.remaining);
                startActivity(intent);
            }else {
                startActivity(new Intent(MainActivity.this,SubscriptionActivity.class));
                finish();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    private void getSubscriptionDetailInitially(){
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(MainActivity.this,Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_user_subscription.php", params, new MainActivity.GetResponsehandler2());
    }

    public class GetResponsehandler2 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            SubscribeDetailModel2 model = new Gson().fromJson(new String(responseBody), SubscribeDetailModel2.class);
            if (model.success == 1) {
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
            else if(model.success == 2){
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE,model.type);
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTION,"1");
            }else {
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment());
        adapter.addFrag(new CompassFragment());
        adapter.addFrag(new CarouselFragment());
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showLogoutAlert(){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();

        final Typeface font = Typeface.createFromAsset(getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(getAssets(), "paitb0.ttf");

        View dialogView = inflater.inflate(R.layout.logoutalert, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        Button no_btn = (Button) dialogView.findViewById(R.id.no_btn);
        Button logout_btn = (Button) dialogView.findViewById(R.id.logout_btn);
        TextView subs_txt = (TextView) dialogView.findViewById(R.id.logout_txt);
        subs_txt.setTypeface(fontb);

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_ID, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_FIRSTNAME, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_LASTNAME, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_EMAIL, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_DOB, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_COUNTRY, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_STATE, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_CITY, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_GENDER, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_AGE, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTION, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONTYPE, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_SUBSCRIPTIONDAYS, "");
                Utils.saveToUserDefaults(MainActivity.this, Constant.PARAM_VALID_NEWTOAPP, "");

                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();
            }
        });



        alertDialog.show();
    }

    private void sendNotification(String body, String title) {
        Intent notificationIntent = new Intent(this, com.shadesix.pdf.QuoteActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.putExtra("notification", body);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notification =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(body)
                                .setBigContentTitle(title))
                        .setContentText(body)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notification.build());
    }

}
