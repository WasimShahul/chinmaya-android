package com.shadesix.pdf;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.models.QuoteModel;
import com.shadesix.pdf.models.QuoteModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    TextView quotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (Utils.getFromUserDefaults(SplashScreenActivity.this, Constant.PARAM_VALID_NEWTOAPP).isEmpty() || Utils.getFromUserDefaults(SplashScreenActivity.this, Constant.PARAM_VALID_NEWTOAPP).equals(null)) {
                    Utils.saveToUserDefaults(SplashScreenActivity.this, Constant.PARAM_VALID_NEWTOAPP, "1");
                    Intent i = new Intent(SplashScreenActivity.this, OnboardingActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            QuoteModel2 model = new Gson().fromJson(new String(responseBody), QuoteModel2.class);
            if (model.success == 1) {
                try {
                    for (QuoteModel mydealmodel : model.quote) {
                        quotes.setText(mydealmodel.content);
                    }
                } catch (Exception e) {

                }
            } else {
                Toast.makeText(getApplicationContext(), model.message, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(getBaseContext(), "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

}

