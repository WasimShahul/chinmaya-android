package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import es.voghdev.pdfviewpager.library.PDFViewPager;
import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;
import pl.droidsonroids.gif.GifTextView;


public class PdfViewerActivityWasim extends AppCompatActivity implements DownloadFile.Listener {
    LinearLayout root;
    RemotePDFViewPager remotePDFViewPager;
    Button btnDownload;
    String magazine_path;
    PDFPagerAdapter adapter;
    Toolbar toolbar = null;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("CU");
        setContentView(R.layout.activity_remote_pdf);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        root = (LinearLayout) findViewById(R.id.remote_pdf_root);
        btnDownload = (Button) findViewById(R.id.btn_download);
        magazine_path = getIntent().getStringExtra("magazine_path");
        setDownloadButtonListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.close();
        }
    }

    protected void setDownloadButtonListener() {
        try{
            final Context ctx = this;
            final DownloadFile.Listener listener = this;
            remotePDFViewPager = new RemotePDFViewPager(ctx, "http://www.chinmayaudghosh.com/admin/"+magazine_path, listener);
            Log.d("pdfview","pdf view "+"http://www.chinmayaudghosh.com/admin/"+magazine_path);
            remotePDFViewPager.setId(R.id.pdfViewPager);
            btnDownload.setVisibility(View.GONE);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            progressDialog.setCancelable(false);
        } catch (Exception e){

        }

    }

    public static void open(Context context) {
        try{
            Intent i = new Intent(context, PdfViewerActivityWasim.class);
            context.startActivity(i);
        } catch (Exception e){

        }

    }

    public void hideDownloadButton() {
        progressDialog.dismiss();
        btnDownload.setVisibility(View.GONE);
    }

    public void updateLayout() {
        try{
//            root.removeAllViewsInLayout();
//            root.addView(btnDownload,
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            root.addView(remotePDFViewPager,
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            root.addView(toolbar,
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e){

        }

    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        try{
            adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(adapter);
            updateLayout();
            hideDownloadButton();
        } catch (Exception e){

        }

    }

    @Override
    public void onFailure(Exception e) {
        try{
            e.printStackTrace();
        } catch(Exception ec){

        }

    }

    @Override
    public void onProgressUpdate(int progress, int total) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            startActivity(new Intent(PdfViewerActivityWasim.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}