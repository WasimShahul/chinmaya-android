package com.shadesix.pdf;

/**
 * Created by marco.granatiero on 03/02/2015.
 */
public class GameEntity {
  public int imageResId;
  public String titleResId;

  public GameEntity (int imageResId, String titleResId){
    this.imageResId = imageResId;
    this.titleResId = titleResId;
  }
}