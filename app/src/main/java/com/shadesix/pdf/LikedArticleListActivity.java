package com.shadesix.pdf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.shadesix.pdf.adapters.LikedArticleAdapter;
import com.shadesix.pdf.models.LikedModel;
import com.shadesix.pdf.models.LikedModel2;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

public class LikedArticleListActivity extends AppCompatActivity {

    LikedArticleAdapter dealadapter;
    public static ArrayList<LikedModel> likeList = new ArrayList<LikedModel>();
    RecyclerView likedList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liked_article_list);

        likedList = (RecyclerView) findViewById(R.id.liked_list);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        getLikedList();
    }

    public void getLikedList(){
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id", Utils.getFromUserDefaults(LikedArticleListActivity.this, Constant.PARAM_VALID_ID));
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_liked_articles.php", params, new LikedArticleListActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            LikedModel2 model = new Gson().fromJson(new String(responseBody), LikedModel2.class);
            likeList.clear();
            if (model.success == 1) {
                try {
                    for (LikedModel mydealmodel : model.likedDetail) {
                        likeList.add(mydealmodel);
                        dealadapter = new LikedArticleAdapter(likeList, LikedArticleListActivity.this);
                    }
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(LikedArticleListActivity.this, 1);
                    likedList.setLayoutManager(mLayoutManager);
                    likedList.setAdapter(dealadapter);

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(LikedArticleListActivity.this,model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(LikedArticleListActivity.this,"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(LikedArticleListActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
