package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.activity.WebViewActivity;
import com.shadesix.pdf.adapters.GetSubscriptionAdapter;
import com.shadesix.pdf.adapters.MagazineAdapter;
import com.shadesix.pdf.models.GetSubscriptionModel;
import com.shadesix.pdf.models.GetSubscriptionModel2;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.models.SubscriptionModel;
import com.shadesix.pdf.utils.AvenuesParams;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.ServiceUtility;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SubscriptionActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    static String TAG = SubscriptionActivity.class.getSimpleName();
    public static ArrayList<GetSubscriptionModel> homeMagList = new ArrayList<GetSubscriptionModel>();
    public GetSubscriptionAdapter getSubscriptionAdapter;
    public RecyclerView subscription_list;
    public Button btn_subscribe_now;
    public static String stype, sduration, samount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        progressDialog = new ProgressDialog(this);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        subscription_list = (RecyclerView) findViewById(R.id.subscription_list);
        btn_subscribe_now = (Button) findViewById(R.id.btn_subscribe_now);
        Log.e(TAG, "user id " + Utils.getFromUserDefaults(SubscriptionActivity.this, Constant.PARAM_VALID_ID));

        getAllSubscriptions();


    }

    public void setbuttonvisibility() {
        btn_subscribe_now.setVisibility(View.VISIBLE);
    }

    public void selectedSubsType(String type, String duration, String amount) {

        stype = type;
        sduration = duration;
        samount = amount;

    }

    public void clickNextFunc(String type, String duration, String amount) {

        stype = type;
        sduration = duration;
        samount = amount;
        moveToNextActivity(stype, sduration, samount);


    }

    public void moveToNextActivity(final String subscriptionType, final String subscriptionDuration, final String subscriptionAmount) {

        btn_subscribe_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.getFromUserDefaults(SubscriptionActivity.this, Constant.PARAM_VALID_ID).isEmpty()) {
                    Intent intent = new Intent(SubscriptionActivity.this, LoginActivity.class);
                    intent.putExtra("from_act", "Subscription");
                    startActivity(intent);
                } else {


                    String vAccessCode = "AVSN70EE54AD65NSDA";
                    String vMerchantId = "132266";
                    String vCurrency;
                    if (Utils.getFromUserDefaults(SubscriptionActivity.this, Constant.PARAM_VALID_COUNTRY).toLowerCase().trim().equals("india")) {
                        vCurrency = "INR";
                    } else {
                        vCurrency = "USD";
                    }

                    String vAmount = subscriptionAmount;
                    Integer randomNum = ServiceUtility.randInt(0, 9999999);
                    if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
                        Intent intent = new Intent(SubscriptionActivity.this, WebViewActivity.class);
                        intent.putExtra(AvenuesParams.ACCESS_CODE, vAccessCode);
                        intent.putExtra(AvenuesParams.MERCHANT_ID, vMerchantId);
                        intent.putExtra(AvenuesParams.ORDER_ID, randomNum.toString());
                        intent.putExtra(AvenuesParams.CURRENCY, vCurrency);
                        intent.putExtra(AvenuesParams.AMOUNT, vAmount);
                        intent.putExtra("subscription_type", stype);
                        intent.putExtra("subscription_duration", sduration);
                        intent.putExtra("subscription_cost", samount);
                        intent.putExtra("subscription_orderid", randomNum.toString());

                        intent.putExtra(AvenuesParams.REDIRECT_URL, "http://www.chinmayaudghosh.com/api/ccavenue/ccavResponseHandler.php");
                        intent.putExtra(AvenuesParams.CANCEL_URL, "http://www.chinmayaudghosh.com/api/ccavenue/ccavResponseHandler.php");
                        intent.putExtra(AvenuesParams.RSA_KEY_URL, "http://www.chinmayaudghosh.com/api/ccavenue/GetRSA.php");

                        startActivity(intent);
                    } else {
                        Toast.makeText(getBaseContext(), "All parameters mandatory", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }

    private void getAllSubscriptions() {
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.post("http://www.chinmayaudghosh.com/api/get_all_subscription.php", params, new SubscriptionActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            GetSubscriptionModel2 model = new Gson().fromJson(new String(responseBody), GetSubscriptionModel2.class);
            Log.e(TAG, "success value " + model.success);
            if (model.success == 1) {
                homeMagList.clear();
                try {
                    for (GetSubscriptionModel mm : model.subcriptionDetail) {
                        homeMagList.add(mm);
                        getSubscriptionAdapter = new GetSubscriptionAdapter(homeMagList, SubscriptionActivity.this);

                    }
                    LinearLayoutManager llm = new LinearLayoutManager(SubscriptionActivity.this);
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    subscription_list.setLayoutManager(llm);
                    subscription_list.setAdapter(getSubscriptionAdapter);
                } catch (Exception ex) {
                    Toast.makeText(SubscriptionActivity.this, "Make sure you selected radio buttons", Toast.LENGTH_LONG).show();
                }
            } else {
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(SubscriptionActivity.this, "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(SubscriptionActivity.this, MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SubscriptionActivity.this, MainActivity.class));
        finish();
    }
}
