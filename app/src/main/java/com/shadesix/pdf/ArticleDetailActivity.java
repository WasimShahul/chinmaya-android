package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.adapters.ArticleAdapter;
import com.shadesix.pdf.models.GetArticleModel;
import com.shadesix.pdf.models.GetArticleModel2;
import com.shadesix.pdf.models.GetIsLikedModel;
import com.shadesix.pdf.models.LikesModel;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArticleDetailActivity extends AppCompatActivity {
    static String TAG = ArticleDetailActivity.class.getSimpleName();
    private Toolbar toolbar;
    ImageView fontInc,fontDec,likes, img_main,article_share;
    TextView title,description,numLikes,auth_tv,pub_tv, cp, txt_title, txt_subs, lastread_txt;
    float fontSize1 = 18;
    ProgressDialog progressDialog;
    float fontSize = 22;
    String article_id,num,author,pubDate,isLiked,para,heading, count1;
    public static ArrayList<GetArticleModel> artList = new ArrayList<GetArticleModel>();
    ImageView back_btn;
    LinearLayout likesll, sharell;
    private static boolean isliked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_article_detail);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        final Typeface font = Typeface.createFromAsset(getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(getAssets(), "paitb0.ttf");

        progressDialog = new ProgressDialog(this);
        fontInc = (ImageView) findViewById(R.id.increase_toolbar);
        fontDec = (ImageView) findViewById(R.id.decrease_toolbar);
        likes = (ImageView) findViewById(R.id.img_likes);
        title = (TextView) findViewById(R.id.txt_desc_title);
        auth_tv = (TextView) findViewById(R.id.txt_author_name);
        pub_tv = (TextView) findViewById(R.id.txt_pub_date);
        numLikes = (TextView) findViewById(R.id.txt_likes);
        description = (TextView) findViewById(R.id.txt_desc_para);
        cp = (TextView) findViewById(R.id.cp);
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_subs = (TextView) findViewById(R.id.txt_subs);
        lastread_txt = (TextView) findViewById(R.id.lastread_txt);
        img_main = (ImageView) findViewById(R.id.img_main);
        article_share = (ImageView) findViewById(R.id.article_share);
        likesll = (LinearLayout) findViewById(R.id.likesll);
        sharell = (LinearLayout) findViewById(R.id.sharell);
        article_id = getIntent().getStringExtra("article_id");
        num = getIntent().getStringExtra("num_likes");
        author = getIntent().getStringExtra("author");
        pubDate = getIntent().getStringExtra("pub_date");
        isLiked = getIntent().getStringExtra("is_liked");
        para = getIntent().getStringExtra("para");
        count1 = getIntent().getStringExtra("count1");

        heading = getIntent().getStringExtra("title");
        back_btn = (ImageView) findViewById(R.id.back_btn);

        Log.e(TAG,"article id "+article_id);
        Log.e(TAG,"isLiked "+isLiked);

        getArticles();
//        checkLikes();

        numLikes.setText(num+" likes");
        auth_tv.setText(author);
        pub_tv.setText(pubDate);
        if(Utils.getFromUserDefaults(ArticleDetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).isEmpty() || Utils.getFromUserDefaults(ArticleDetailActivity.this,Constant.PARAM_VALID_SUBSCRIPTIONDAYS).equals("0")){
           try {
               if(count1.equals("12")){
                   txt_subs.setVisibility(View.GONE);
               } else{
                   description.setMaxLines(10);
                   txt_subs.setVisibility(View.VISIBLE);
               }
           }catch (Exception e){
               description.setMaxLines(10);
               txt_subs.setVisibility(View.VISIBLE);
           }
        } else{
            txt_subs.setVisibility(View.GONE);
        }
        description.setTypeface(font);
        title.setTypeface(fontb);
        txt_subs.setTypeface(font);
        cp.setTypeface(font);
        txt_title.setTypeface(fontb);
        lastread_txt.setTypeface(fontb);
        lastread_txt.setVisibility(View.GONE);
        description.setText(Html.fromHtml(para));
        title.setText(heading);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fontInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fontSize >= 26){

                }else {
                    fontSize = fontSize + 2;
                }
                if(fontSize1 >= 22){

                }else {
                    fontSize1 = fontSize1 + 2;
                }

                title.setTextSize(fontSize);
                description.setTextSize(fontSize1);

            }
        });

        sharell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String sAux = "\nLet me recommend you this application\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=com.shadesix.pdf \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "Share With"));
                } catch(Exception e) {
                }

            }
        });

        fontDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(fontSize <= 22){

                }else {
                    fontSize = fontSize - 2;
                }
                if(fontSize1 <= 18){

                }else {
                    fontSize1 = fontSize1 - 2;
                }

                title.setTextSize(fontSize);
                description.setTextSize(fontSize1);
            }
        });

        likesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.getFromUserDefaults(ArticleDetailActivity.this, Constant.PARAM_VALID_ID).isEmpty()){
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ArticleDetailActivity.this);
                    builder1.setMessage("Please login to like your favourite articles");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(ArticleDetailActivity.this, com.shadesix.pdf.LoginActivity.class);
                                    intent.putExtra("from_act","main");
                                    dialog.cancel();
                                    startActivity(intent);
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }else{
                    saveLikes();
                }
            }
        });
    }

    private void saveLikes(){

//        progressDialog.setMessage("Loading...");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id",Utils.getFromUserDefaults(ArticleDetailActivity.this,Constant.PARAM_VALID_ID));
        params.put("article_id", article_id);
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/article_like.php", params, new ArticleDetailActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//            progressDialog.dismiss();
            LikesModel model = new Gson().fromJson(new String(responseBody), LikesModel.class);
            if (model.success == 1) {
                try {
                    getArticles();
                    numLikes.setText(model.count+" likes");
                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    private void checkLikes(){

        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("user_id",Utils.getFromUserDefaults(ArticleDetailActivity.this,Constant.PARAM_VALID_ID));
        params.put("article_id", article_id);
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/is_liked.php", params, new ArticleDetailActivity.GetResponsehandler1());
    }

    public class GetResponsehandler1 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            GetIsLikedModel model = new Gson().fromJson(new String(responseBody), GetIsLikedModel.class);
            if (model.success == 1) {
                try {
                    Log.e("checklikes", model.is_liked);
                    if(model.is_liked.equals("1")){
                        likes.setImageResource(R.drawable.red_heart);
                        isliked = true;
                    } else {
                        likes.setImageResource(R.drawable.heart_button);
                        isliked = false;
                    }
                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    public void getArticles(){
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_articles_by_magazine.php?user_id="+Utils.getFromUserDefaults(ArticleDetailActivity.this,Constant.PARAM_VALID_ID), params, new ArticleDetailActivity.GetResponsehandler2());
    }

    public class GetResponsehandler2 extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//            progressDialog.dismiss();
            GetArticleModel2 model = new Gson().fromJson(new String(responseBody), GetArticleModel2.class);
            if (model.success == 1) {
                try {
                    for (GetArticleModel gam : model.articleDetail) {
                        if(article_id.equals(gam.id)){
                            if(gam.is_liked.equals("0")){
                                likes.setImageResource(R.drawable.heart_button);
                                numLikes.setText(gam.article_likes+" likes");
                            } else  {
                                likes.setImageResource(R.drawable.red_heart);
                                numLikes.setText(gam.article_likes+" likes");
                            }

                            if(gam.is_read.equals("0")){
                                lastread_txt.setVisibility(View.GONE);
                            } else  {
                                lastread_txt.setVisibility(View.VISIBLE);
                            }

                            txt_title.setText(gam.title);

                            Picasso.with(ArticleDetailActivity.this).load(Constant.PARAM_VALID_BASE_URL+gam.cover_path).into(img_main);


                        } else{

                        }
                    }


                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(ArticleDetailActivity.this,model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
//            progressDialog.dismiss();
            Toast.makeText(ArticleDetailActivity.this,"Unable to get article title list.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
