package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shadesix.pdf.models.ForgotPassModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.shadesix.pdf.LoginActivity;
import com.shadesix.pdf.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class  VerifyOtpActivity extends AppCompatActivity {

    TextView dynamic;
    EditText otp,pass,confirm;
    Button submit;
    String text;
    ProgressDialog progressDialog;
    public String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        text = getIntent().getStringExtra("mail");

        progressDialog = new ProgressDialog(this);

        submit = (Button) findViewById(R.id.btn_submit);
        otp = (EditText) findViewById(R.id.edt_otp);
        pass = (EditText) findViewById(R.id.edt_new_pass);
        confirm = (EditText) findViewById(R.id.edt_confirm_pass);
        dynamic = (TextView) findViewById(R.id.txt_dynamic);

        dynamic.setText("An OTP has been sent to "+text);

//        pass.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                String passStr = pass.getText().toString();
//                Pattern p = Pattern.compile("[^A-Za-z0-9@$&/\\*!_]");
//                Matcher m = p.matcher(passStr);
//
//                boolean b = m.find();
//
//                if (b == true || passStr.length()< 8){
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.RED);
//                    ViewCompat.setBackgroundTintList(pass, colorStateList);
//                }else if(passStr.length() >= 8){
//                    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor("#06AF3A"));
//                    ViewCompat.setBackgroundTintList(pass, colorStateList);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//
//
//
//            }
//        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(VerifyOtpActivity.this);
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    saveNewPass();
                }
            }
        });
    }



    private void saveNewPass(){
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.put("otp", otp.getText().toString());
        params.put("password", pass.getText().toString());
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/validate_forgot_password.php", params, new VerifyOtpActivity.GetResponsehandler());
    }

    public boolean validate() {
        boolean isValidate = false;
        if (pass.getText().toString().trim().isEmpty() || confirm.getText().toString().trim().isEmpty() || otp.getText().toString().trim().isEmpty() ) {
            isValidate = false;
            message = "Please enter all the fields";
        } else if(!validatePassword(pass.getText().toString())){
            isValidate = false;
            message = "Please enter a valid Password";
        } else if (pass.getText().toString().length() < 8) {
            isValidate = false;
            message = "Password must be minimum of 8 characters";
        } else if (!pass.getText().toString().equals(confirm.getText().toString())) {
            isValidate = false;
            message = "Passwords do not match";
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    public boolean validatePassword(String password) {
        String ePattern = "^[A-Za-z0-9@$&/\\*!_]+$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(password);
        return m.matches();
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            ForgotPassModel model = new Gson().fromJson(new String(responseBody), ForgotPassModel.class);
            progressDialog.dismiss();
            if (model.success == 1) {
                try {
                    Toast.makeText(VerifyOtpActivity.this,"Password Changed Successfully",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(VerifyOtpActivity.this,LoginActivity.class));
                    finish();

                }catch (Exception e){

                }
            }
            else{
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Invalid OTP",Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
