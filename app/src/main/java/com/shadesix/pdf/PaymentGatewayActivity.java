package com.shadesix.pdf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.shadesix.pdf.models.SubscriptionModel;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PaymentGatewayActivity extends AppCompatActivity {

    static String TAG = PaymentGatewayActivity.class.getSimpleName();
    String radioText,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gateway);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Payment Gateway");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        radioText = getIntent().getStringExtra("plan");
        email = getIntent().getStringExtra("mail");
        Log.e(TAG,"radio text "+radioText);


        saveToServer();
    }

    private void saveToServer() {
        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyymmdd");
        Date today = cal.getTime();
        Date nextYear;

        if(radioText.equals("1")){
            cal.add(Calendar.YEAR, 1);
            nextYear = cal.getTime();
        }
        else if(radioText.equals("2")){
            cal.add(Calendar.YEAR, 2);
            nextYear = cal.getTime();
        }
        else {
            cal.add(Calendar.YEAR,3);
            nextYear = cal.getTime();
        }

        String date = df.format(today);
        String next = df.format(nextYear);

        Log.e(TAG,"today date "+date);
        Log.e(TAG,"next date "+next);
//        Toast.makeText(getBaseContext(),"Please Wait...Saving your data on a secured server.", Toast.LENGTH_LONG).show();
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();

        params.put("subType",radioText);
        params.put("startDate",date);
        params.put("endDate",next);
        params.put("email", email);
        Log.e(TAG,"email" + Utils.getFromUserDefaults(PaymentGatewayActivity.this, Constant.PARAM_VALID_EMAIL));
//        params.put("areaoflaw", law.getText().toString());

        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/user_subscription.php", params, new PaymentGatewayActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            SubscriptionModel model = new Gson().fromJson(new String(responseBody), SubscriptionModel.class);
            if (model.success == 1) {
                Toast.makeText(PaymentGatewayActivity.this,model.message,Toast.LENGTH_LONG).show();
                try{
//

                } catch (Exception ex){
                    Toast.makeText(PaymentGatewayActivity.this,"Make sure you selected radio buttons",Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(PaymentGatewayActivity.this,model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(PaymentGatewayActivity.this,"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(PaymentGatewayActivity.this,SubscriptionActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
