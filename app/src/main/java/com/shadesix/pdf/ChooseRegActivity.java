package com.shadesix.pdf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;


public class ChooseRegActivity extends AppCompatActivity {

    Button login,reg;
    String subscribe_plan,fromAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_reg);

        login = (Button) findViewById(R.id.btn_login);
        reg = (Button) findViewById(R.id.btn_reg);

        subscribe_plan = getIntent().getStringExtra("plan");
        fromAct = getIntent().getStringExtra("from_act");

        try {
            if(!Utils.getFromUserDefaults(ChooseRegActivity.this, Constant.PARAM_VALID_EMAIL).isEmpty()){
                Intent intent = new Intent(ChooseRegActivity.this,PaymentGatewayActivity.class);
                intent.putExtra("plan",subscribe_plan);
                intent.putExtra("mail",Utils.getFromUserDefaults(ChooseRegActivity.this, Constant.PARAM_VALID_EMAIL));
                startActivity(intent);
                finish();
            }
        }catch (Exception ex){

        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseRegActivity.this,LoginActivity.class);
                intent.putExtra("plan",subscribe_plan);
                intent.putExtra("from_act",fromAct);
                startActivity(intent);
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChooseRegActivity.this,RegisterationActivity.class);
                intent.putExtra("plan",subscribe_plan);
                intent.putExtra("from_act",fromAct);
                startActivity(intent);
            }
        });
    }
}
