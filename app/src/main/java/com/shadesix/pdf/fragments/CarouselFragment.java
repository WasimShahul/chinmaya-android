package com.shadesix.pdf.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.R;
import com.shadesix.pdf.adapters.CustPagerTransformer;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.models.MagazineModel2;
import com.shadesix.pdf.utils.Constant;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CarouselFragment extends Fragment {

    private TextView indicatorTv;
    private View positionView;
    View rootView;
    private ViewPager viewPager;
    ProgressDialog progressDialog;
    private List<CommonFragment> fragments = new ArrayList<>(); // 供ViewPager使用
    private String[] imageArray = {"assets://august.png"};
    private int count;

    public CarouselFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_carousel, container, false);
        indicatorTv = (TextView) rootView.findViewById(R.id.indicator_tv);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        progressDialog = new ProgressDialog(getContext());
        positionView = rootView.findViewById(R.id.position_view);

        dealStatusBar();
        fillViewPager();
        return rootView;
    }

    private void fillViewPager() {


        // 1. viewPager添加parallax效果，使用PageTransformer就足够了
        viewPager.setPageTransformer(false, new CustPagerTransformer(getContext()));

        //Get JSON RESPONSE
        getAllMagazines();


    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        indicatorTv.setText(Html.fromHtml("<font color='#12edf0'>" + currentItem + "</font>  /  " + totalNum));
    }

    private void dealStatusBar() {

    }

    public void getAllMagazines() {
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        client.get("http://www.chinmayaudghosh.com/api/get_all_magazines.php", params, new CarouselFragment.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            MagazineModel2 model = new Gson().fromJson(new String(responseBody), MagazineModel2.class);
            Log.d("carousel", "response body is " + responseBody);
            count = 0;
            if (model.success == 1) {
                try {
                    fragments.clear();
                    for (MagazineModel mydealmodel : model.magazineDetail) {
                        if (count <= 12) {
                            if (count == 12) {
                                fragments.add(0, new CommonFragment(mydealmodel, count));
                            } else {
                                fragments.add(new CommonFragment(mydealmodel, count));
                            }
                            Log.d("carousel", count + " " + Constant.PARAM_VALID_BASE_URL + mydealmodel.cover_path);
                            count = ++count;
                        }

                    }

                    viewPager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
                        @Override
                        public Fragment getItem(int position) {
                            CommonFragment fragment = fragments.get(position % count);
                            fragment.bindData(imageArray[position % imageArray.length]);
                            return fragment;
                        }

                        @Override
                        public int getCount() {
                            return count;
                        }
                    });


                    // 3. viewPager滑动时，调整指示器
                    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(int position) {
                            updateIndicatorTv();
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

                    updateIndicatorTv();

                } catch (Exception e) {

                }
            } else {
                Toast.makeText(getActivity(), model.message, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }


    private int getStatusBarHeight() {
        Class<?> c = null;
        Object obj = null;
        Field field = null;
        int x = 0, statusBarHeight = 0;
        try {
            c = Class.forName("com.android.internal.R$dimen");
            obj = c.newInstance();
            field = c.getField("status_bar_height");
            x = Integer.parseInt(field.get(obj).toString());
            statusBarHeight = getResources().getDimensionPixelSize(x);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return statusBarHeight;
    }

    @SuppressWarnings("deprecation")
    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .memoryCacheExtraOptions(480, 800)
                // default = device screen dimensions
                .threadPoolSize(3)
                // default
                .threadPriority(Thread.NORM_PRIORITY - 1)
                // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13) // default
                .discCacheSize(50 * 1024 * 1024) // 缓冲大小
                .discCacheFileCount(100) // 缓冲文件数目
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(getActivity())) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs().build();

        // 2.单例ImageLoader类的初始化
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }
}
