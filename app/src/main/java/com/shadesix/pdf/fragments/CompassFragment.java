package com.shadesix.pdf.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shadesix.pdf.R;
import com.shadesix.pdf.adapters.ExploreAdapter;
import com.shadesix.pdf.models.ExploreModel;
import com.shadesix.pdf.models.ExploreModel2;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;


public class CompassFragment extends Fragment {

    RecyclerView exploreList;
    ExploreAdapter dealadapter;
    public int count = 0;
    ProgressDialog progressDialog;
    public static ArrayList<ExploreModel> articleTitleList = new ArrayList<ExploreModel>();
    public CompassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_compass, container,
                false);
        exploreList = (RecyclerView) view.findViewById(R.id.explore_list);
        progressDialog = new ProgressDialog(getContext());
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        getArticleList();

        return view;
    }

    public void getArticleList(){
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_articles.php", params, new CompassFragment.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            ExploreModel2 model = new Gson().fromJson(new String(responseBody), ExploreModel2.class);
            if (model.success == 1) {
                try {
                    for (ExploreModel atl : model.exploreModel) {
                        articleTitleList.add(atl);
                        dealadapter = new ExploreAdapter(articleTitleList, getActivity(), count);
                        count = ++count;
                    }

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                    exploreList.setLayoutManager(mLayoutManager);
                    exploreList.setAdapter(dealadapter);

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getActivity(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"Unable to get article title list.", Toast.LENGTH_LONG).show();
        }
    }

}
