package com.shadesix.pdf.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.shadesix.pdf.DetailActivity;
import com.shadesix.pdf.GetAllArticleListActivity;
import com.shadesix.pdf.MagazineViewActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.adapters.DragLayout;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.utils.Constant;
import com.squareup.picasso.Picasso;

/**
 * Created by xmuSistone on 2016/9/18.
 */
public class CommonFragment extends Fragment implements DragLayout.GotoDetailListener {
    private ImageView imageView;
    private TextView address1, address3, address4, address5;
    private RatingBar ratingBar;
    private View head1, head2, head3, head4;
    private String imageUrl;
    public static String str;
    public MagazineModel magazinemodel;
    public String count1;

    public CommonFragment() {

    }

    @SuppressLint("ValidFragment")
    public CommonFragment(MagazineModel md, int count) {
        magazinemodel = md;
        count1 = count+"";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_common, container,false);
        DragLayout dragLayout = (DragLayout) rootView.findViewById(R.id.drag_layout);
        imageView = (ImageView) dragLayout.findViewById(R.id.image);

        try {
            Picasso.with(getContext()).load(Constant.PARAM_VALID_BASE_URL+magazinemodel.cover_path).into(imageView);
        } catch (Exception e){

        }


//        ImageLoader.getInstance().displayImage(imageUrl, imageView);
        address1 = (TextView) dragLayout.findViewById(R.id.address1);
        address1.setText("add1");

        address3 = (TextView) dragLayout.findViewById(R.id.address3);
        address3.setText("add3");

        address4 = (TextView) dragLayout.findViewById(R.id.address4);
        try{
            if(count1.equals("12")){
                address4.setText(magazinemodel.title+"\n FREE ISSUE \n Issue: "+magazinemodel.issue+"\n Volume: "+magazinemodel.volume);
            } else {
                address4.setText(magazinemodel.title+"\n Issue: "+magazinemodel.issue+"\n Volume: "+magazinemodel.volume);
            }
        } catch(Exception e){
            address4.setText(magazinemodel.title+"\n Issue: "+magazinemodel.issue+"\n Volume: "+magazinemodel.volume);
        }

        address5 = (TextView) dragLayout.findViewById(R.id.address5);
        address5.setText("Issue: "+magazinemodel.issue+"\n Volume: "+magazinemodel.volume);

        dragLayout.setGotoDetailListener(this);
        return rootView;
    }

    @Override
    public void gotoDetail() {
        Activity activity = (Activity) getContext();
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                new Pair(imageView, DetailActivity.EXTRA_IMAGE_URL),
                new Pair(address1, DetailActivity.ADDRESS1_TRANSITION_NAME),
                new Pair(address3, DetailActivity.ADDRESS3_TRANSITION_NAME),
                new Pair(address4, DetailActivity.ADDRESS4_TRANSITION_NAME),
                new Pair(address5, DetailActivity.ADDRESS5_TRANSITION_NAME)
        );
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("magazine_id", magazinemodel.id);
        intent.putExtra("magazine_path", magazinemodel.pdf_path);
        intent.putExtra("pubDate",magazinemodel.publishing_date);
        intent.putExtra("magTitle",magazinemodel.title);
        intent.putExtra("MagVol",magazinemodel.volume);
        intent.putExtra("magIssue",magazinemodel.issue);
        intent.putExtra("magCover",magazinemodel.cover_path);
        intent.putExtra("count1",count1);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    public void bindData(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
