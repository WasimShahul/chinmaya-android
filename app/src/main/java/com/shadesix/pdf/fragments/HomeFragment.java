package com.shadesix.pdf.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.DetailActivity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.adapters.MagazineAdapter;
import com.shadesix.pdf.models.MagazineModel;
import com.shadesix.pdf.models.MagazineModel2;
import com.shadesix.pdf.utils.Constant;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    public static String TAG = HomeFragment.class.getSimpleName();
    RecyclerView lvspec;
    ImageView imageView;
    String featuredImage, featuredDate, featuremagIssue, featureMagVol, featurePubDetail, featuremagTitle;
    String featureCoverImage;
    String featuredId;
    String pubDetail;
    String magTitle;
    String MagVol;
    String magIssue;
    MagazineAdapter dealadapter;
    TextView txt_main_title;
    public static ArrayList<MagazineModel> homeMagList = new ArrayList<MagazineModel>();
    ProgressDialog progressDialog;
    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container,
                false);
        lvspec = (RecyclerView) rootView.findViewById(R.id.home_magazine_list);
        imageView = (ImageView) rootView.findViewById(R.id.img_main);
        txt_main_title = (TextView) rootView.findViewById(R.id.txt_main_title);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               gotoDetail();
            }
        });
        final Typeface font = Typeface.createFromAsset(getContext().getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(getContext().getAssets(), "paitb0.ttf");
        txt_main_title.setTypeface(fontb);
        progressDialog = new ProgressDialog(getContext());
        getMagazines();
        return rootView;
    }

    public void getMagazines(){
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_all_magazines.php", params, new HomeFragment.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            MagazineModel2 model = new Gson().fromJson(new String(responseBody), MagazineModel2.class);
            if (model.success == 1) {
                try {
                    for (MagazineModel mm : model.magazineDetail) {
                        homeMagList.add(mm);
                        pubDetail = mm.publisher;
                        magTitle = mm.title;
                        MagVol = mm.volume;
                        magIssue = mm.issue;
                        dealadapter = new MagazineAdapter(homeMagList, getActivity());
                    }

                    for(int i=0; i<homeMagList.size(); i++){
                        featuredImage = homeMagList.get(0).cover_path;
                        featureCoverImage = homeMagList.get(0).pdf_path;
                        featuredId = homeMagList.get(0).id;
                        featuredDate =homeMagList.get(0).title;
                        featurePubDetail =homeMagList.get(0).publishing_date;
                        featureMagVol =homeMagList.get(0).volume;
                        featuremagIssue =homeMagList.get(0).issue;
                        featuremagTitle = homeMagList.get(0).title;
                    }

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                    lvspec.setLayoutManager(mLayoutManager);
                    lvspec.setAdapter(dealadapter);

                    Picasso.with(getContext()).load(Constant.PARAM_VALID_BASE_URL+featuredImage).into(imageView);

                    txt_main_title.setText(featuredDate+"\n INSPIRING YOUTH TO LOOK AHEAD");

                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getActivity(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getActivity(),"Unable to get article title list.", Toast.LENGTH_LONG).show();
        }
    }

    public void gotoDetail() {
        Activity activity = (Activity) getContext();
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                new Pair(imageView, DetailActivity.EXTRA_IMAGE_URL)
        );
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("magazine_id", featuredId);
        intent.putExtra("magazine_path", featureCoverImage);
        intent.putExtra("pubDate",featurePubDetail);
        intent.putExtra("magTitle",featuremagTitle);
        intent.putExtra("MagVol",featureMagVol);
        intent.putExtra("magIssue",featuremagIssue);
        intent.putExtra("magCover",featuredImage);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

}
