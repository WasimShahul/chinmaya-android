package com.shadesix.pdf.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextSwitcher;

import com.shadesix.pdf.GameEntity;
import com.shadesix.pdf.R;
import com.shadesix.pdf.TestActivity;
import com.shadesix.pdf.adapters.CoverFlowAdapter;

import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


public class ThirdFragment extends Fragment {

    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    private ArrayList<GameEntity> mData = new ArrayList<>(0);
    private TextSwitcher mTitle;
    View rootView;
    ImageView squareimg;
    public ThirdFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_third, container,false);
        squareimg = (ImageView) rootView.findViewById(R.id.squareimg);
//         mCoverFlow = (FeatureCoverFlow) rootView.findViewById(R.id.coverflow);
//                    mTitle = (TextSwitcher) rootView.findViewById(R.id.title);
        squareimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TestActivity.class));
            }
        });
//        mData.add(new GameEntity(R.drawable.jan, "Title1"));
//        mData.add(new GameEntity(R.drawable.feb, "Title2"));
//        mData.add(new GameEntity(R.drawable.march, "Title3"));
//        mData.add(new GameEntity(R.drawable.april, "Title4"));
//        mData.add(new GameEntity(R.drawable.may, "Title5"));
//        mData.add(new GameEntity(R.drawable.june, "Title6"));
//        mData.add(new GameEntity(R.drawable.july, "Title6"));
//        mData.add(new GameEntity(R.drawable.august, "Title6"));
//        mData.add(new GameEntity(R.drawable.september, "Title6"));
//        mData.add(new GameEntity(R.drawable.october, "Title6"));
//        mData.add(new GameEntity(R.drawable.november, "Title6"));
//        mData.add(new GameEntity(R.drawable.december, "Title6"));

//        mAdapter = new CoverFlowAdapter(getContext());
//        mAdapter.setData(mData);
//        mCoverFlow = (FeatureCoverFlow) rootView.findViewById(R.id.coverflow);
//        mCoverFlow.setAdapter(mAdapter);

//        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//        });
//
//        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
//            @Override
//            public void onScrolledToPosition(int position) {
////                mTitle.setText(getResources().getString(mData.get(position).titleResId));
//            }
//
//            @Override
//            public void onScrolling() {
////                mTitle.setText("");
//            }
//        });


        try{
//            getAllMagazines();
//            mAdapter = new CoverFlowAdapter(getContext());
        } catch (Exception e){

        }


        return rootView;
    }

//    public void getAllMagazines() {
//        Toast.makeText(getActivity(), "Getting Magazine", Toast.LENGTH_LONG).show();
//        AsyncHttpClient client = new AsyncHttpClient();
//        RequestParams params = new RequestParams();
//        client.get("http://www.chinmayaudghosh.com/api/get_all_magazines.php", params, new ThirdFragment.GetResponsehandler());
//    }

//    public class GetResponsehandler extends AsyncHttpResponseHandler {
//
//        @Override
//        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//            MagazineModel2 model = new Gson().fromJson(new String(responseBody), MagazineModel2.class);
//            Log.d("coverfragment","response body is "+responseBody);
//            if (model.success == 1) {
//                try {
//                    magList.clear();
//                    for (MagazineModel mydealmodel : model.magazineDetail) {
//                        magList.add(mydealmodel);
//                    }
//
//                    mAdapter.setData(magList);
////                    mCoverFlow.setAdapter(mAdapter);
//                    mTitle.setFactory(new ViewSwitcher.ViewFactory() {
//                        @Override
//                        public View makeView() {
//                            LayoutInflater inflater = LayoutInflater.from(getActivity());
//                            TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
//                            return textView;
//                        }
//                    });
//
//                    Animation in = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
//                    Animation out = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_bottom);
//                    mTitle.setInAnimation(in);
//                    mTitle.setOutAnimation(out);
//
//
//
////                    mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////                        @Override
////                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                            Toast.makeText(getActivity(), magList.get(position).publisher, Toast.LENGTH_SHORT).show();
////
//////                Intent intent = new Intent(getActivity(), AndroidPageCurlActivity.class);
//////                intent.putExtra("magazine_id",magList.get(position).id);
//////                startActivity(intent);
////                            Intent intent = new Intent(getActivity(), PDFCurlViewAct.class);
////                            intent.putExtra( "PDFAsset", "test.PDF" );
////                            intent.putExtra( "PDFPswd", "" );//password
////                            startActivity(intent);
////                        }
////                    });
////
////                    mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
////                        @Override
////                        public void onScrolledToPosition(int position) {
//////                mTitle.setText(magList.get(position).publishing_date);
////                        }
////
////                        @Override
////                        public void onScrolling() {
////                            mTitle.setText("");
////                        }
////                    });
//
//                }catch (Exception e){
//
//                }
//            }
//            else{
//                Toast.makeText(getActivity(),model.message,Toast.LENGTH_LONG).show();
//            }
//        }
//
//        @Override
//        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
//            Toast.makeText(getActivity(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
//        }
//    }


}
