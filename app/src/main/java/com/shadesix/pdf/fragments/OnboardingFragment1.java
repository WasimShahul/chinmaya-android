package com.shadesix.pdf.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shadesix.pdf.R;

public class OnboardingFragment1 extends Fragment {

    public TextView sen1, sen2, sen3, sen4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.onboarding_screen1, container,
                false);

        final Typeface font = Typeface.createFromAsset(getContext().getAssets(), "paitr0.ttf");
        Typeface fontb = Typeface.createFromAsset(getContext().getAssets(), "paitb0.ttf");

        sen1 = (TextView) rootView.findViewById(R.id.sen1);
        sen2 = (TextView) rootView.findViewById(R.id.sen2);
        sen3 = (TextView) rootView.findViewById(R.id.sen3);
        sen4 = (TextView) rootView.findViewById(R.id.sen4);

        sen1.setTypeface(fontb);
        sen2.setTypeface(fontb);
        sen3.setTypeface(fontb);
        sen4.setTypeface(fontb);

        return rootView;
    }
}
