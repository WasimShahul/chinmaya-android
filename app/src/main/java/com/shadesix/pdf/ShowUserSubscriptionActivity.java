package com.shadesix.pdf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class ShowUserSubscriptionActivity extends AppCompatActivity {

    static String TAG = ShowUserSubscriptionActivity.class.getSimpleName();
    TextView subDate,endDate,subType;
    String type,remainingDays;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_user_subscription);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        type = getIntent().getStringExtra("type");
        remainingDays = getIntent().getStringExtra("remaining");

        subDate = (TextView) findViewById(R.id.txt_sub_date);
        endDate = (TextView) findViewById(R.id.txt_end_date);

        subDate.setText("You are already subscribed in "+type+" year plan");
        endDate.setText("Your subscription is still valid for "+remainingDays+" days");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(ShowUserSubscriptionActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
