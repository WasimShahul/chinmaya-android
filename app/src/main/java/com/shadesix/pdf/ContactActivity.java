package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.shadesix.pdf.activity.StatusActivity;
import com.shadesix.pdf.utils.Constant;
import com.shadesix.pdf.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ContactActivity extends AppCompatActivity {

    EditText name,email,message;
    Button submit;
    ProgressDialog progressDialog;
    String message1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        submit = (Button) findViewById(R.id.contact_button);
        name = (EditText) findViewById(R.id.contact_name);
        email = (EditText) findViewById(R.id.contact_email);
        message = (EditText) findViewById(R.id.contact_message);
        progressDialog = new ProgressDialog(this);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    sendContactUs();
                } else {
                    Toast.makeText(ContactActivity.this,message1,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean validate(){
        boolean isValidate = false;
        if (name.getText().toString().trim().isEmpty() || email.getText().toString().trim().isEmpty() || message.getText().toString().trim().isEmpty() ) {
            isValidate = false;
            message1 = "Please enter all the fields";
        } else if (!validateEmail(email.getText().toString())) {
            isValidate = false;
            message1 = "Please enter a valid email id";
        } else if(!validateName(name.getText().toString())){
            isValidate = false;
            message1 = "Please enter a valid first name";
        } else {
            isValidate = true;
        }
        return isValidate;
    }

    public boolean validateEmail(String mailId) {
        String ePattern = "^[a-zA-Z0-9.!$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(mailId);
        return m.matches();
    }

    public boolean validateName(String name) {
        String ePattern = "^[\\p{L} .'-]+$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(name);
        return m.matches();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(ContactActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ContactActivity.this,MainActivity.class));
        finish();
    }

    public void sendContactUs(){
        try {
        progressDialog.setMessage("Sending...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        params.add("name",name.getText().toString());
        params.add("mail",email.getText().toString());
        params.add("message",message.getText().toString());

        client.setTimeout(DEFAULT_TIMEOUT);
        String url = null;

            url = "http://www.chinmayaudghosh.com/api/contact.php?name="+name.getText().toString()+"&mail="+email.getText().toString()+"&message="+message.getText().toString();
            url = url.replaceAll(" " ,"%20");
//            url = URLEncoder.encode("http://www.chinmayaudghosh.com/api/contact.php?name="+name.getText().toString()+"&mail="+email.getText().toString()+"&message="+message.getText().toString(), java.nio.charset.StandardCharsets.UTF_8.toString());
            client.post(url, params, new GetResponsehandler());
        } catch (Exception e) {
            e.printStackTrace();

        }
//        client.post("http://www.chinmayaudghosh.com/api/contact.php?name="+name.getText().toString()+"&mail="+email.getText().toString()+"&message="+message.getText().toString(), params, new GetResponsehandler());

    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            Intent intent = new Intent(ContactActivity.this, MainActivity.class);
            startActivity(intent);
            Toast.makeText(ContactActivity.this,"Thank you for contacting with us",Toast.LENGTH_LONG).show();
        }
        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(ContactActivity.this,"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }
}
