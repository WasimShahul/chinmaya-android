package com.shadesix.pdf;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.shadesix.pdf.models.QuoteModel;
import com.shadesix.pdf.models.QuoteModel2;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class QuoteActivity extends AppCompatActivity {

    TextView quotes;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);

        quotes = (TextView) findViewById(R.id.txt_quote);
        progressDialog = new ProgressDialog(this);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        getQuotes();
    }

    public void getQuotes(){
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("http://www.chinmayaudghosh.com/api/get_daily_quote.php", params, new QuoteActivity.GetResponsehandler());
    }

    public class GetResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            progressDialog.dismiss();
            QuoteModel2 model = new Gson().fromJson(new String(responseBody), QuoteModel2.class);
            if (model.success == 1) {
                try {
                    for (QuoteModel mydealmodel : model.quote) {
                        quotes.setText(mydealmodel.content);
                    }
                }catch (Exception e){

                }
            }
            else{
                Toast.makeText(getApplicationContext(),model.message,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            progressDialog.dismiss();
            Toast.makeText(getBaseContext(),"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startActivity(new Intent(QuoteActivity.this,MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(QuoteActivity.this,MainActivity.class));
        finish();
    }
}
