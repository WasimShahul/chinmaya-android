package com.shadesix.pdf.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.shadesix.pdf.PlacesAutoCompleteAdapter;
import com.shadesix.pdf.R;
import com.shadesix.pdf.RecyclerItemClickListener;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class SelectAddressActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    protected GoogleApiClient mGoogleApiClient;

    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(-0, 0), new LatLng(0, 0));

    Geocoder geocoder;
    List<Address> addresses;
    Double addrLat,addrLng;
    LatLng addrLatLng;
    public static EditText mAutocompleteView;
    static String addrState;
    private RecyclerView mRecyclerView;
    Button addr_ok;
    String fromClass;
    RelativeLayout root;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    ImageView delete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        setContentView(R.layout.activity_select_address);

        fromClass = getIntent().getStringExtra("fromClass");
        mAutocompleteView = (EditText)findViewById(R.id.autocomplete_places);

        delete=(ImageView)findViewById(R.id.cross);

        mAutoCompleteAdapter =  new PlacesAutoCompleteAdapter(this, R.layout.searchview_adapter,
                mGoogleApiClient, BOUNDS_INDIA, null);

        mRecyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        addr_ok=(Button) findViewById(R.id.btn_ok);
        root=(RelativeLayout) findViewById(R.id.root_rel);

        overrideFonts(SelectAddressActivity.this,root);


        mLinearLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);
        delete.setOnClickListener(this);
        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                }else if(!mGoogleApiClient.isConnected()){
                    Toast.makeText(getApplicationContext(), "API_NOT_CONNECTED",Toast.LENGTH_SHORT).show();
                }
                mRecyclerView.setVisibility(View.VISIBLE);
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                mRecyclerView.setVisibility(View.VISIBLE);
            }

            public void afterTextChanged(Editable s) {

            }
        });

        addr_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mAutocompleteView.getText().toString().isEmpty()){
                    Toast.makeText(SelectAddressActivity.this,"Please Select your Address",Toast.LENGTH_SHORT).show();
                }else if(fromClass.equals("Registration")) {
                    try {
//                        String cit = addresses.get(0).getLocality();
//                        String stat = addresses.get(0).getAdminArea();
//                        String postalCode = addresses.get(0).getPostalCode();
//                        Intent intent = new Intent(SelectAddressActivity.this, Registration.class);
//                        intent.putExtra("address", mAutocompleteView.getText().toString());
//                        intent.putExtra("state", stat);
//                        intent.putExtra("city", cit);
//                        intent.putExtra("pincode", postalCode);
//                        intent.putExtra("latitude", addrLat);
//                        intent.putExtra("longitude", addrLng);
//                        intent.putExtra("fromClass","SelectAddressAct");
//                        startActivity(intent);
//                        finish();
                    }catch (Exception e){
                        Toast.makeText(SelectAddressActivity.this,"Try Again",Toast.LENGTH_SHORT).show();
                    }

                }else {
                    try {
//                        String cit = addresses.get(0).getLocality();
//                        String stat = addresses.get(0).getAdminArea();
//                        String postalCode = addresses.get(0).getPostalCode();
//                        Intent intent = new Intent(SelectAddressActivity.this, UserDetailActivity.class);
//                        intent.putExtra("address", mAutocompleteView.getText().toString());
//                        intent.putExtra("state", stat);
//                        intent.putExtra("city", cit);
//                        intent.putExtra("pincode", postalCode);
//                        intent.putExtra("latitude", addrLat);
//                        intent.putExtra("longitude", addrLng);
//                        intent.putExtra("fromClass","SelectAddressAct");
//                        startActivity(intent);
//                        finish();
                    } catch (Exception e){
                        Toast.makeText(SelectAddressActivity.this,"Try Again",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try{
                            final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                            final String placeId = String.valueOf(item.placeId);
                            Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */

                            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                    .getPlaceById(mGoogleApiClient, placeId);
                            placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    if(places.getCount()==1){
                                        //Do the things here on Click.....
                                        mAutocompleteView.setText(places.get(0).getAddress());
                                        geocoder = new Geocoder(SelectAddressActivity.this, Locale.getDefault());
                                        addrLatLng = places.get(0).getLatLng();
                                        addrLat = addrLatLng.latitude;
                                        addrLng = addrLatLng.longitude;
                                        try {
                                            addresses = geocoder.getFromLocation(addrLat, addrLng, 1);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

//                                    Toast.makeText(getApplicationContext(),String.valueOf(places.get(0).getLatLng()),Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_SHORT).show();
                                    }
                                    mRecyclerView.setVisibility(View.GONE);
                                }
                            });
                            Log.i("TAG", "Clicked: " + item.description);
                            Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                        } catch (Exception e){
                            Toast.makeText(SelectAddressActivity.this,"Try Again",Toast.LENGTH_SHORT).show();
                        }

                    }
                })
        );
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback","Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
        Toast.makeText(this, "API NOT CONNECTED",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if(v==delete){
            mAutocompleteView.setText("");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()){
            Log.v("Google API","Connecting");
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected()){
            Log.v("Google API","Dis-Connecting");
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(fromClass.equals("Registration")) {
//            Intent intent = new Intent(SelectAddressActivity.this, Registration.class);
//            intent.putExtra("fromClass", "select_addr");
//            startActivity(intent);
//            finish();
        }else {
//            Intent intent = new Intent(SelectAddressActivity.this, UserDetailActivity.class);
//            intent.putExtra("fromClass", "select_addr");
//            startActivity(intent);
//            finish();
        }
    }

    private void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "poppins_light.ttf"));
            }
        } catch (Exception e) {
        }
    }
}
