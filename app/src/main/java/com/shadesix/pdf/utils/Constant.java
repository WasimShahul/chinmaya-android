package com.shadesix.pdf.utils;

/**
 * Created by Shade Six on 28-03-2017.
 */

public class Constant {
    public final static String SHARED_PREFS = "CUApp";
    public final static String PARAM_VALID_ID = "valid_id";
    public final static String PARAM_VALID_FIRSTNAME = "valid_first_name";
    public final static String PARAM_VALID_LASTNAME = "valid_last_name";
    public final static String PARAM_VALID_EMAIL = "valid_email";
    public final static String PARAM_VALID_DOB = "valid_dob";
    public final static String PARAM_VALID_COUNTRY = "valid_country";
    public final static String PARAM_VALID_STATE = "valid_state";
    public final static String PARAM_VALID_CITY = "valid_city";
    public final static String PARAM_VALID_GENDER = "valid_gender";
    public final static String PARAM_VALID_AGE = "valid_age";
    public final static String PARAM_VALID_BASE_URL = "http://www.chinmayaudghosh.com/admin/";
    public final static String PARAM_VALID_QUOTES = "valid_quotes";
    public final static String PARAM_VALID_SUBSCRIPTION = "valid_subscription";
    public final static String PARAM_VALID_SUBSCRIPTIONTYPE = "valid_subscriptiontype";
    public final static String PARAM_VALID_SUBSCRIPTIONDAYS = "valid_subscriptiondays";
    public final static String PARAM_VALID_NEWTOAPP = "valid_newtoapp";
}
